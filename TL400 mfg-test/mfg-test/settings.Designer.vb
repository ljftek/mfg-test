﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class settings
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.TextBox_PWR_PCT = New System.Windows.Forms.TextBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.TextBox_VISA_ID = New System.Windows.Forms.TextBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.TextBox_TRIGGER_VOLTAGE = New System.Windows.Forms.TextBox()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.TextBox_UUT_TX_GAIN = New System.Windows.Forms.TextBox()
        Me.TextBox_BRG_TX_GAIN = New System.Windows.Forms.TextBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.TextBox_LOGFILE = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.TextBox_COM_PORT = New System.Windows.Forms.TextBox()
        Me.TextBox_COLOR_0_MIN = New System.Windows.Forms.TextBox()
        Me.TextBox_COLOR_0_MAX = New System.Windows.Forms.TextBox()
        Me.TextBox_COLOR_25_MIN = New System.Windows.Forms.TextBox()
        Me.TextBox_COLOR_25_MAX = New System.Windows.Forms.TextBox()
        Me.TextBox_COLOR_50_MIN = New System.Windows.Forms.TextBox()
        Me.TextBox_COLOR_50_MAX = New System.Windows.Forms.TextBox()
        Me.TextBox_COLOR_75_MIN = New System.Windows.Forms.TextBox()
        Me.TextBox_COLOR_75_MAX = New System.Windows.Forms.TextBox()
        Me.TextBox_COLOR_100_MIN = New System.Windows.Forms.TextBox()
        Me.TextBox_COLOR_100_MAX = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.textBox_LOG_ADDRESS = New System.Windows.Forms.TextBox()
        Me.textBox_LIGHT_0_MIN = New System.Windows.Forms.TextBox()
        Me.textBox_LIGHT_0_MAX = New System.Windows.Forms.TextBox()
        Me.textBox_LIGHT_25_MIN = New System.Windows.Forms.TextBox()
        Me.textBox_LIGHT_25_MAX = New System.Windows.Forms.TextBox()
        Me.textBox_LIGHT_50_MIN = New System.Windows.Forms.TextBox()
        Me.textBox_LIGHT_50_MAX = New System.Windows.Forms.TextBox()
        Me.textBox_LIGHT_75_MIN = New System.Windows.Forms.TextBox()
        Me.textBox_LIGHT_75_MAX = New System.Windows.Forms.TextBox()
        Me.textBox_LIGHT_100_MIN = New System.Windows.Forms.TextBox()
        Me.textBox_LIGHT_100_MAX = New System.Windows.Forms.TextBox()
        Me.textBox_AMP_TEST_PASS_VAL = New System.Windows.Forms.TextBox()
        Me.textBox_ECHO_PASS_NUM = New System.Windows.Forms.TextBox()
        Me.textBox_NUM_ECHO_TEST = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.TextBox_PWR_PCT)
        Me.GroupBox1.Controls.Add(Me.Label31)
        Me.GroupBox1.Controls.Add(Me.TextBox_VISA_ID)
        Me.GroupBox1.Controls.Add(Me.Label30)
        Me.GroupBox1.Controls.Add(Me.TextBox_TRIGGER_VOLTAGE)
        Me.GroupBox1.Controls.Add(Me.Label29)
        Me.GroupBox1.Controls.Add(Me.TextBox_UUT_TX_GAIN)
        Me.GroupBox1.Controls.Add(Me.TextBox_BRG_TX_GAIN)
        Me.GroupBox1.Controls.Add(Me.Label27)
        Me.GroupBox1.Controls.Add(Me.Label28)
        Me.GroupBox1.Controls.Add(Me.Label26)
        Me.GroupBox1.Controls.Add(Me.TextBox_LOGFILE)
        Me.GroupBox1.Controls.Add(Me.Label25)
        Me.GroupBox1.Controls.Add(Me.TextBox_COM_PORT)
        Me.GroupBox1.Controls.Add(Me.TextBox_COLOR_0_MIN)
        Me.GroupBox1.Controls.Add(Me.TextBox_COLOR_0_MAX)
        Me.GroupBox1.Controls.Add(Me.TextBox_COLOR_25_MIN)
        Me.GroupBox1.Controls.Add(Me.TextBox_COLOR_25_MAX)
        Me.GroupBox1.Controls.Add(Me.TextBox_COLOR_50_MIN)
        Me.GroupBox1.Controls.Add(Me.TextBox_COLOR_50_MAX)
        Me.GroupBox1.Controls.Add(Me.TextBox_COLOR_75_MIN)
        Me.GroupBox1.Controls.Add(Me.TextBox_COLOR_75_MAX)
        Me.GroupBox1.Controls.Add(Me.TextBox_COLOR_100_MIN)
        Me.GroupBox1.Controls.Add(Me.TextBox_COLOR_100_MAX)
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.Label16)
        Me.GroupBox1.Controls.Add(Me.Label17)
        Me.GroupBox1.Controls.Add(Me.Label18)
        Me.GroupBox1.Controls.Add(Me.Label19)
        Me.GroupBox1.Controls.Add(Me.Label20)
        Me.GroupBox1.Controls.Add(Me.Label21)
        Me.GroupBox1.Controls.Add(Me.Label22)
        Me.GroupBox1.Controls.Add(Me.Label23)
        Me.GroupBox1.Controls.Add(Me.Label24)
        Me.GroupBox1.Controls.Add(Me.textBox_LOG_ADDRESS)
        Me.GroupBox1.Controls.Add(Me.textBox_LIGHT_0_MIN)
        Me.GroupBox1.Controls.Add(Me.textBox_LIGHT_0_MAX)
        Me.GroupBox1.Controls.Add(Me.textBox_LIGHT_25_MIN)
        Me.GroupBox1.Controls.Add(Me.textBox_LIGHT_25_MAX)
        Me.GroupBox1.Controls.Add(Me.textBox_LIGHT_50_MIN)
        Me.GroupBox1.Controls.Add(Me.textBox_LIGHT_50_MAX)
        Me.GroupBox1.Controls.Add(Me.textBox_LIGHT_75_MIN)
        Me.GroupBox1.Controls.Add(Me.textBox_LIGHT_75_MAX)
        Me.GroupBox1.Controls.Add(Me.textBox_LIGHT_100_MIN)
        Me.GroupBox1.Controls.Add(Me.textBox_LIGHT_100_MAX)
        Me.GroupBox1.Controls.Add(Me.textBox_AMP_TEST_PASS_VAL)
        Me.GroupBox1.Controls.Add(Me.textBox_ECHO_PASS_NUM)
        Me.GroupBox1.Controls.Add(Me.textBox_NUM_ECHO_TEST)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(26, 20)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(477, 492)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'TextBox_PWR_PCT
        '
        Me.TextBox_PWR_PCT.Location = New System.Drawing.Point(122, 158)
        Me.TextBox_PWR_PCT.Name = "TextBox_PWR_PCT"
        Me.TextBox_PWR_PCT.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_PWR_PCT.TabIndex = 62
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(3, 161)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(99, 13)
        Me.Label31.TabIndex = 61
        Me.Label31.Text = "Energy Deviation %"
        '
        'TextBox_VISA_ID
        '
        Me.TextBox_VISA_ID.Location = New System.Drawing.Point(367, 45)
        Me.TextBox_VISA_ID.Name = "TextBox_VISA_ID"
        Me.TextBox_VISA_ID.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_VISA_ID.TabIndex = 60
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(248, 48)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(45, 13)
        Me.Label30.TabIndex = 59
        Me.Label30.Text = "VISA ID"
        '
        'TextBox_TRIGGER_VOLTAGE
        '
        Me.TextBox_TRIGGER_VOLTAGE.Location = New System.Drawing.Point(122, 45)
        Me.TextBox_TRIGGER_VOLTAGE.Name = "TextBox_TRIGGER_VOLTAGE"
        Me.TextBox_TRIGGER_VOLTAGE.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_TRIGGER_VOLTAGE.TabIndex = 58
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(3, 48)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(79, 13)
        Me.Label29.TabIndex = 57
        Me.Label29.Text = "Trigger Voltage"
        '
        'TextBox_UUT_TX_GAIN
        '
        Me.TextBox_UUT_TX_GAIN.Location = New System.Drawing.Point(367, 71)
        Me.TextBox_UUT_TX_GAIN.Name = "TextBox_UUT_TX_GAIN"
        Me.TextBox_UUT_TX_GAIN.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_UUT_TX_GAIN.TabIndex = 56
        '
        'TextBox_BRG_TX_GAIN
        '
        Me.TextBox_BRG_TX_GAIN.Location = New System.Drawing.Point(122, 71)
        Me.TextBox_BRG_TX_GAIN.Name = "TextBox_BRG_TX_GAIN"
        Me.TextBox_BRG_TX_GAIN.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_BRG_TX_GAIN.TabIndex = 55
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(248, 74)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(74, 13)
        Me.Label27.TabIndex = 54
        Me.Label27.Text = "Echo TX Gain"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(3, 74)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(95, 13)
        Me.Label28.TabIndex = 53
        Me.Label28.Text = "Amplitude TX Gain"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(248, 22)
        Me.Label26.Name = "Label26"
        Me.Label26.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label26.Size = New System.Drawing.Size(67, 13)
        Me.Label26.TabIndex = 52
        Me.Label26.Text = "Log filename"
        '
        'TextBox_LOGFILE
        '
        Me.TextBox_LOGFILE.Location = New System.Drawing.Point(367, 19)
        Me.TextBox_LOGFILE.Name = "TextBox_LOGFILE"
        Me.TextBox_LOGFILE.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_LOGFILE.TabIndex = 51
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(248, 109)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(86, 13)
        Me.Label25.TabIndex = 50
        Me.Label25.Text = "RS485 Com Port"
        '
        'TextBox_COM_PORT
        '
        Me.TextBox_COM_PORT.Location = New System.Drawing.Point(367, 106)
        Me.TextBox_COM_PORT.Name = "TextBox_COM_PORT"
        Me.TextBox_COM_PORT.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_COM_PORT.TabIndex = 49
        '
        'TextBox_COLOR_0_MIN
        '
        Me.TextBox_COLOR_0_MIN.Location = New System.Drawing.Point(367, 462)
        Me.TextBox_COLOR_0_MIN.Name = "TextBox_COLOR_0_MIN"
        Me.TextBox_COLOR_0_MIN.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_COLOR_0_MIN.TabIndex = 48
        '
        'TextBox_COLOR_0_MAX
        '
        Me.TextBox_COLOR_0_MAX.Location = New System.Drawing.Point(367, 436)
        Me.TextBox_COLOR_0_MAX.Name = "TextBox_COLOR_0_MAX"
        Me.TextBox_COLOR_0_MAX.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_COLOR_0_MAX.TabIndex = 47
        '
        'TextBox_COLOR_25_MIN
        '
        Me.TextBox_COLOR_25_MIN.Location = New System.Drawing.Point(367, 410)
        Me.TextBox_COLOR_25_MIN.Name = "TextBox_COLOR_25_MIN"
        Me.TextBox_COLOR_25_MIN.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_COLOR_25_MIN.TabIndex = 46
        '
        'TextBox_COLOR_25_MAX
        '
        Me.TextBox_COLOR_25_MAX.Location = New System.Drawing.Point(367, 384)
        Me.TextBox_COLOR_25_MAX.Name = "TextBox_COLOR_25_MAX"
        Me.TextBox_COLOR_25_MAX.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_COLOR_25_MAX.TabIndex = 45
        '
        'TextBox_COLOR_50_MIN
        '
        Me.TextBox_COLOR_50_MIN.Location = New System.Drawing.Point(367, 359)
        Me.TextBox_COLOR_50_MIN.Name = "TextBox_COLOR_50_MIN"
        Me.TextBox_COLOR_50_MIN.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_COLOR_50_MIN.TabIndex = 44
        '
        'TextBox_COLOR_50_MAX
        '
        Me.TextBox_COLOR_50_MAX.Location = New System.Drawing.Point(367, 334)
        Me.TextBox_COLOR_50_MAX.Name = "TextBox_COLOR_50_MAX"
        Me.TextBox_COLOR_50_MAX.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_COLOR_50_MAX.TabIndex = 43
        '
        'TextBox_COLOR_75_MIN
        '
        Me.TextBox_COLOR_75_MIN.Location = New System.Drawing.Point(367, 308)
        Me.TextBox_COLOR_75_MIN.Name = "TextBox_COLOR_75_MIN"
        Me.TextBox_COLOR_75_MIN.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_COLOR_75_MIN.TabIndex = 42
        '
        'TextBox_COLOR_75_MAX
        '
        Me.TextBox_COLOR_75_MAX.Location = New System.Drawing.Point(367, 282)
        Me.TextBox_COLOR_75_MAX.Name = "TextBox_COLOR_75_MAX"
        Me.TextBox_COLOR_75_MAX.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_COLOR_75_MAX.TabIndex = 41
        '
        'TextBox_COLOR_100_MIN
        '
        Me.TextBox_COLOR_100_MIN.Location = New System.Drawing.Point(367, 256)
        Me.TextBox_COLOR_100_MIN.Name = "TextBox_COLOR_100_MIN"
        Me.TextBox_COLOR_100_MIN.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_COLOR_100_MIN.TabIndex = 40
        '
        'TextBox_COLOR_100_MAX
        '
        Me.TextBox_COLOR_100_MAX.Location = New System.Drawing.Point(367, 230)
        Me.TextBox_COLOR_100_MAX.Name = "TextBox_COLOR_100_MAX"
        Me.TextBox_COLOR_100_MAX.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_COLOR_100_MAX.TabIndex = 39
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(248, 259)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(90, 13)
        Me.Label15.TabIndex = 38
        Me.Label15.Text = "Color 100 Min Val"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(248, 311)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(84, 13)
        Me.Label16.TabIndex = 37
        Me.Label16.Text = "Color 75 Min Val"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(248, 362)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(84, 13)
        Me.Label17.TabIndex = 36
        Me.Label17.Text = "Color 50 Min Val"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(248, 413)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(84, 13)
        Me.Label18.TabIndex = 35
        Me.Label18.Text = "Color 25 Min Val"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(248, 465)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(78, 13)
        Me.Label19.TabIndex = 34
        Me.Label19.Text = "Color 0 Min Val"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(248, 233)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(93, 13)
        Me.Label20.TabIndex = 33
        Me.Label20.Text = "Color 100 Max Val"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(248, 285)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(87, 13)
        Me.Label21.TabIndex = 32
        Me.Label21.Text = "Color 75 Max Val"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(248, 337)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(87, 13)
        Me.Label22.TabIndex = 31
        Me.Label22.Text = "Color 50 Max Val"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(248, 387)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(87, 13)
        Me.Label23.TabIndex = 30
        Me.Label23.Text = "Color 25 Max Val"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(248, 439)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(81, 13)
        Me.Label24.TabIndex = 29
        Me.Label24.Text = "Color 0 Max Val"
        '
        'textBox_LOG_ADDRESS
        '
        Me.textBox_LOG_ADDRESS.Location = New System.Drawing.Point(122, 19)
        Me.textBox_LOG_ADDRESS.Name = "textBox_LOG_ADDRESS"
        Me.textBox_LOG_ADDRESS.Size = New System.Drawing.Size(100, 20)
        Me.textBox_LOG_ADDRESS.TabIndex = 28
        '
        'textBox_LIGHT_0_MIN
        '
        Me.textBox_LIGHT_0_MIN.Location = New System.Drawing.Point(122, 462)
        Me.textBox_LIGHT_0_MIN.Name = "textBox_LIGHT_0_MIN"
        Me.textBox_LIGHT_0_MIN.Size = New System.Drawing.Size(100, 20)
        Me.textBox_LIGHT_0_MIN.TabIndex = 27
        '
        'textBox_LIGHT_0_MAX
        '
        Me.textBox_LIGHT_0_MAX.Location = New System.Drawing.Point(122, 436)
        Me.textBox_LIGHT_0_MAX.Name = "textBox_LIGHT_0_MAX"
        Me.textBox_LIGHT_0_MAX.Size = New System.Drawing.Size(100, 20)
        Me.textBox_LIGHT_0_MAX.TabIndex = 25
        '
        'textBox_LIGHT_25_MIN
        '
        Me.textBox_LIGHT_25_MIN.Location = New System.Drawing.Point(122, 410)
        Me.textBox_LIGHT_25_MIN.Name = "textBox_LIGHT_25_MIN"
        Me.textBox_LIGHT_25_MIN.Size = New System.Drawing.Size(100, 20)
        Me.textBox_LIGHT_25_MIN.TabIndex = 24
        '
        'textBox_LIGHT_25_MAX
        '
        Me.textBox_LIGHT_25_MAX.Location = New System.Drawing.Point(122, 384)
        Me.textBox_LIGHT_25_MAX.Name = "textBox_LIGHT_25_MAX"
        Me.textBox_LIGHT_25_MAX.Size = New System.Drawing.Size(100, 20)
        Me.textBox_LIGHT_25_MAX.TabIndex = 23
        '
        'textBox_LIGHT_50_MIN
        '
        Me.textBox_LIGHT_50_MIN.Location = New System.Drawing.Point(122, 359)
        Me.textBox_LIGHT_50_MIN.Name = "textBox_LIGHT_50_MIN"
        Me.textBox_LIGHT_50_MIN.Size = New System.Drawing.Size(100, 20)
        Me.textBox_LIGHT_50_MIN.TabIndex = 22
        '
        'textBox_LIGHT_50_MAX
        '
        Me.textBox_LIGHT_50_MAX.Location = New System.Drawing.Point(122, 334)
        Me.textBox_LIGHT_50_MAX.Name = "textBox_LIGHT_50_MAX"
        Me.textBox_LIGHT_50_MAX.Size = New System.Drawing.Size(100, 20)
        Me.textBox_LIGHT_50_MAX.TabIndex = 21
        '
        'textBox_LIGHT_75_MIN
        '
        Me.textBox_LIGHT_75_MIN.Location = New System.Drawing.Point(122, 308)
        Me.textBox_LIGHT_75_MIN.Name = "textBox_LIGHT_75_MIN"
        Me.textBox_LIGHT_75_MIN.Size = New System.Drawing.Size(100, 20)
        Me.textBox_LIGHT_75_MIN.TabIndex = 20
        '
        'textBox_LIGHT_75_MAX
        '
        Me.textBox_LIGHT_75_MAX.Location = New System.Drawing.Point(122, 282)
        Me.textBox_LIGHT_75_MAX.Name = "textBox_LIGHT_75_MAX"
        Me.textBox_LIGHT_75_MAX.Size = New System.Drawing.Size(100, 20)
        Me.textBox_LIGHT_75_MAX.TabIndex = 19
        '
        'textBox_LIGHT_100_MIN
        '
        Me.textBox_LIGHT_100_MIN.Location = New System.Drawing.Point(122, 256)
        Me.textBox_LIGHT_100_MIN.Name = "textBox_LIGHT_100_MIN"
        Me.textBox_LIGHT_100_MIN.Size = New System.Drawing.Size(100, 20)
        Me.textBox_LIGHT_100_MIN.TabIndex = 18
        '
        'textBox_LIGHT_100_MAX
        '
        Me.textBox_LIGHT_100_MAX.Location = New System.Drawing.Point(122, 230)
        Me.textBox_LIGHT_100_MAX.Name = "textBox_LIGHT_100_MAX"
        Me.textBox_LIGHT_100_MAX.Size = New System.Drawing.Size(100, 20)
        Me.textBox_LIGHT_100_MAX.TabIndex = 17
        '
        'textBox_AMP_TEST_PASS_VAL
        '
        Me.textBox_AMP_TEST_PASS_VAL.Location = New System.Drawing.Point(122, 106)
        Me.textBox_AMP_TEST_PASS_VAL.Name = "textBox_AMP_TEST_PASS_VAL"
        Me.textBox_AMP_TEST_PASS_VAL.Size = New System.Drawing.Size(100, 20)
        Me.textBox_AMP_TEST_PASS_VAL.TabIndex = 16
        '
        'textBox_ECHO_PASS_NUM
        '
        Me.textBox_ECHO_PASS_NUM.Location = New System.Drawing.Point(367, 132)
        Me.textBox_ECHO_PASS_NUM.Name = "textBox_ECHO_PASS_NUM"
        Me.textBox_ECHO_PASS_NUM.Size = New System.Drawing.Size(100, 20)
        Me.textBox_ECHO_PASS_NUM.TabIndex = 15
        '
        'textBox_NUM_ECHO_TEST
        '
        Me.textBox_NUM_ECHO_TEST.Location = New System.Drawing.Point(122, 132)
        Me.textBox_NUM_ECHO_TEST.Name = "textBox_NUM_ECHO_TEST"
        Me.textBox_NUM_ECHO_TEST.Size = New System.Drawing.Size(100, 20)
        Me.textBox_NUM_ECHO_TEST.TabIndex = 14
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(3, 259)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(89, 13)
        Me.Label14.TabIndex = 13
        Me.Label14.Text = "Light 100 Min Val"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(3, 311)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(83, 13)
        Me.Label13.TabIndex = 12
        Me.Label13.Text = "Light 75 Min Val"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(3, 362)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(83, 13)
        Me.Label12.TabIndex = 11
        Me.Label12.Text = "Light 50 Min Val"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(3, 413)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(83, 13)
        Me.Label11.TabIndex = 10
        Me.Label11.Text = "Light 25 Min Val"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(3, 465)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(77, 13)
        Me.Label10.TabIndex = 9
        Me.Label10.Text = "Light 0 Min Val"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(248, 135)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(68, 13)
        Me.Label9.TabIndex = 8
        Me.Label9.Text = "Echo Pass #"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(3, 109)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(96, 13)
        Me.Label8.TabIndex = 7
        Me.Label8.Text = "Amp Test Pass Val"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(3, 233)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(92, 13)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "Light 100 Max Val"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(3, 285)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(86, 13)
        Me.Label6.TabIndex = 5
        Me.Label6.Text = "Light 75 Max Val"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(3, 337)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(86, 13)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Light 50 Max Val"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(3, 387)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(86, 13)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Light 25 Max Val"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(3, 439)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(80, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Light 0 Max Val"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(3, 22)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(108, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "UUT Logical Address"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(3, 135)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(71, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "# Echo Tests"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(35, 518)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "Cancel"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(418, 518)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 2
        Me.Button2.Text = "OK"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'settings
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(531, 546)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "settings"
        Me.Text = "Settings"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents textBox_LIGHT_0_MIN As TextBox
    Friend WithEvents textBox_LIGHT_0_MAX As TextBox
    Friend WithEvents textBox_LIGHT_25_MIN As TextBox
    Friend WithEvents textBox_LIGHT_25_MAX As TextBox
    Friend WithEvents textBox_LIGHT_50_MIN As TextBox
    Friend WithEvents textBox_LIGHT_50_MAX As TextBox
    Friend WithEvents textBox_LIGHT_75_MIN As TextBox
    Friend WithEvents textBox_LIGHT_75_MAX As TextBox
    Friend WithEvents textBox_LIGHT_100_MIN As TextBox
    Friend WithEvents textBox_LIGHT_100_MAX As TextBox
    Friend WithEvents textBox_AMP_TEST_PASS_VAL As TextBox
    Friend WithEvents textBox_ECHO_PASS_NUM As TextBox
    Friend WithEvents textBox_NUM_ECHO_TEST As TextBox
    Friend WithEvents Label14 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents Button1 As Button
    Friend WithEvents Button2 As Button
    Friend WithEvents textBox_LOG_ADDRESS As TextBox
    Friend WithEvents TextBox_COLOR_0_MIN As TextBox
    Friend WithEvents TextBox_COLOR_0_MAX As TextBox
    Friend WithEvents TextBox_COLOR_25_MIN As TextBox
    Friend WithEvents TextBox_COLOR_25_MAX As TextBox
    Friend WithEvents TextBox_COLOR_50_MIN As TextBox
    Friend WithEvents TextBox_COLOR_50_MAX As TextBox
    Friend WithEvents TextBox_COLOR_75_MIN As TextBox
    Friend WithEvents TextBox_COLOR_75_MAX As TextBox
    Friend WithEvents TextBox_COLOR_100_MIN As TextBox
    Friend WithEvents TextBox_COLOR_100_MAX As TextBox
    Friend WithEvents Label15 As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents Label17 As Label
    Friend WithEvents Label18 As Label
    Friend WithEvents Label19 As Label
    Friend WithEvents Label20 As Label
    Friend WithEvents Label21 As Label
    Friend WithEvents Label22 As Label
    Friend WithEvents Label23 As Label
    Friend WithEvents Label24 As Label
    Friend WithEvents Label25 As Label
    Friend WithEvents TextBox_COM_PORT As TextBox
    Friend WithEvents Label26 As Label
    Friend WithEvents TextBox_LOGFILE As TextBox
    Friend WithEvents TextBox_TRIGGER_VOLTAGE As TextBox
    Friend WithEvents Label29 As Label
    Friend WithEvents TextBox_UUT_TX_GAIN As TextBox
    Friend WithEvents TextBox_BRG_TX_GAIN As TextBox
    Friend WithEvents Label27 As Label
    Friend WithEvents Label28 As Label
    Friend WithEvents TextBox_VISA_ID As TextBox
    Friend WithEvents Label30 As Label
    Friend WithEvents TextBox_PWR_PCT As TextBox
    Friend WithEvents Label31 As Label
End Class
