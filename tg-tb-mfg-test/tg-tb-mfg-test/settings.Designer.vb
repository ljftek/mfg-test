﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class settings
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.TextBox_VISA_ID = New System.Windows.Forms.TextBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.TextBox_TRIGGER_VOLTAGE = New System.Windows.Forms.TextBox()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.TextBox_UUT_TX_GAIN = New System.Windows.Forms.TextBox()
        Me.TextBox_BRG_TX_GAIN = New System.Windows.Forms.TextBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.TextBox_LOGFILE = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.TextBox_COM_PORT = New System.Windows.Forms.TextBox()
        Me.textBox_LOG_ADDRESS = New System.Windows.Forms.TextBox()
        Me.textBox_AMP_TEST_PASS_VAL = New System.Windows.Forms.TextBox()
        Me.textBox_ECHO_PASS_NUM = New System.Windows.Forms.TextBox()
        Me.textBox_NUM_ECHO_TEST = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.TextBox_VISA_ID)
        Me.GroupBox1.Controls.Add(Me.Label30)
        Me.GroupBox1.Controls.Add(Me.TextBox_TRIGGER_VOLTAGE)
        Me.GroupBox1.Controls.Add(Me.Label29)
        Me.GroupBox1.Controls.Add(Me.TextBox_UUT_TX_GAIN)
        Me.GroupBox1.Controls.Add(Me.TextBox_BRG_TX_GAIN)
        Me.GroupBox1.Controls.Add(Me.Label27)
        Me.GroupBox1.Controls.Add(Me.Label28)
        Me.GroupBox1.Controls.Add(Me.Label26)
        Me.GroupBox1.Controls.Add(Me.TextBox_LOGFILE)
        Me.GroupBox1.Controls.Add(Me.Label25)
        Me.GroupBox1.Controls.Add(Me.TextBox_COM_PORT)
        Me.GroupBox1.Controls.Add(Me.textBox_LOG_ADDRESS)
        Me.GroupBox1.Controls.Add(Me.textBox_AMP_TEST_PASS_VAL)
        Me.GroupBox1.Controls.Add(Me.textBox_ECHO_PASS_NUM)
        Me.GroupBox1.Controls.Add(Me.textBox_NUM_ECHO_TEST)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(26, 20)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(477, 210)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'TextBox_VISA_ID
        '
        Me.TextBox_VISA_ID.Location = New System.Drawing.Point(367, 45)
        Me.TextBox_VISA_ID.Name = "TextBox_VISA_ID"
        Me.TextBox_VISA_ID.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_VISA_ID.TabIndex = 60
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(248, 48)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(45, 13)
        Me.Label30.TabIndex = 59
        Me.Label30.Text = "VISA ID"
        '
        'TextBox_TRIGGER_VOLTAGE
        '
        Me.TextBox_TRIGGER_VOLTAGE.Location = New System.Drawing.Point(122, 45)
        Me.TextBox_TRIGGER_VOLTAGE.Name = "TextBox_TRIGGER_VOLTAGE"
        Me.TextBox_TRIGGER_VOLTAGE.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_TRIGGER_VOLTAGE.TabIndex = 58
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(3, 48)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(79, 13)
        Me.Label29.TabIndex = 57
        Me.Label29.Text = "Trigger Voltage"
        '
        'TextBox_UUT_TX_GAIN
        '
        Me.TextBox_UUT_TX_GAIN.Location = New System.Drawing.Point(367, 71)
        Me.TextBox_UUT_TX_GAIN.Name = "TextBox_UUT_TX_GAIN"
        Me.TextBox_UUT_TX_GAIN.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_UUT_TX_GAIN.TabIndex = 56
        '
        'TextBox_BRG_TX_GAIN
        '
        Me.TextBox_BRG_TX_GAIN.Location = New System.Drawing.Point(122, 71)
        Me.TextBox_BRG_TX_GAIN.Name = "TextBox_BRG_TX_GAIN"
        Me.TextBox_BRG_TX_GAIN.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_BRG_TX_GAIN.TabIndex = 55
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(248, 74)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(74, 13)
        Me.Label27.TabIndex = 54
        Me.Label27.Text = "Echo TX Gain"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(3, 74)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(95, 13)
        Me.Label28.TabIndex = 53
        Me.Label28.Text = "Amplitude TX Gain"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(248, 22)
        Me.Label26.Name = "Label26"
        Me.Label26.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label26.Size = New System.Drawing.Size(67, 13)
        Me.Label26.TabIndex = 52
        Me.Label26.Text = "Log filename"
        '
        'TextBox_LOGFILE
        '
        Me.TextBox_LOGFILE.Location = New System.Drawing.Point(367, 19)
        Me.TextBox_LOGFILE.Name = "TextBox_LOGFILE"
        Me.TextBox_LOGFILE.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_LOGFILE.TabIndex = 51
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(248, 109)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(86, 13)
        Me.Label25.TabIndex = 50
        Me.Label25.Text = "RS485 Com Port"
        '
        'TextBox_COM_PORT
        '
        Me.TextBox_COM_PORT.Location = New System.Drawing.Point(367, 106)
        Me.TextBox_COM_PORT.Name = "TextBox_COM_PORT"
        Me.TextBox_COM_PORT.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_COM_PORT.TabIndex = 49
        '
        'textBox_LOG_ADDRESS
        '
        Me.textBox_LOG_ADDRESS.Location = New System.Drawing.Point(122, 19)
        Me.textBox_LOG_ADDRESS.Name = "textBox_LOG_ADDRESS"
        Me.textBox_LOG_ADDRESS.Size = New System.Drawing.Size(100, 20)
        Me.textBox_LOG_ADDRESS.TabIndex = 28
        '
        'textBox_AMP_TEST_PASS_VAL
        '
        Me.textBox_AMP_TEST_PASS_VAL.Location = New System.Drawing.Point(122, 106)
        Me.textBox_AMP_TEST_PASS_VAL.Name = "textBox_AMP_TEST_PASS_VAL"
        Me.textBox_AMP_TEST_PASS_VAL.Size = New System.Drawing.Size(100, 20)
        Me.textBox_AMP_TEST_PASS_VAL.TabIndex = 16
        '
        'textBox_ECHO_PASS_NUM
        '
        Me.textBox_ECHO_PASS_NUM.Location = New System.Drawing.Point(367, 132)
        Me.textBox_ECHO_PASS_NUM.Name = "textBox_ECHO_PASS_NUM"
        Me.textBox_ECHO_PASS_NUM.Size = New System.Drawing.Size(100, 20)
        Me.textBox_ECHO_PASS_NUM.TabIndex = 15
        '
        'textBox_NUM_ECHO_TEST
        '
        Me.textBox_NUM_ECHO_TEST.Location = New System.Drawing.Point(122, 132)
        Me.textBox_NUM_ECHO_TEST.Name = "textBox_NUM_ECHO_TEST"
        Me.textBox_NUM_ECHO_TEST.Size = New System.Drawing.Size(100, 20)
        Me.textBox_NUM_ECHO_TEST.TabIndex = 14
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(248, 135)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(68, 13)
        Me.Label9.TabIndex = 8
        Me.Label9.Text = "Echo Pass #"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(3, 109)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(96, 13)
        Me.Label8.TabIndex = 7
        Me.Label8.Text = "Amp Test Pass Val"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(3, 22)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(108, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "UUT Logical Address"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(3, 135)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(71, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "# Echo Tests"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(35, 236)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "Cancel"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(418, 236)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 2
        Me.Button2.Text = "OK"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'settings
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(531, 265)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "settings"
        Me.Text = "Settings"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents textBox_AMP_TEST_PASS_VAL As TextBox
    Friend WithEvents textBox_ECHO_PASS_NUM As TextBox
    Friend WithEvents textBox_NUM_ECHO_TEST As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents Button1 As Button
    Friend WithEvents Button2 As Button
    Friend WithEvents textBox_LOG_ADDRESS As TextBox
    Friend WithEvents Label25 As Label
    Friend WithEvents TextBox_COM_PORT As TextBox
    Friend WithEvents Label26 As Label
    Friend WithEvents TextBox_LOGFILE As TextBox
    Friend WithEvents TextBox_TRIGGER_VOLTAGE As TextBox
    Friend WithEvents Label29 As Label
    Friend WithEvents TextBox_UUT_TX_GAIN As TextBox
    Friend WithEvents TextBox_BRG_TX_GAIN As TextBox
    Friend WithEvents Label27 As Label
    Friend WithEvents Label28 As Label
    Friend WithEvents TextBox_VISA_ID As TextBox
    Friend WithEvents Label30 As Label
End Class
