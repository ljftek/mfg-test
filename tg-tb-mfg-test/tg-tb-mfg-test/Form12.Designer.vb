﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.ENERGY_RESULTS = New System.Windows.Forms.Label()
        Me.CheckBox_ENERGY = New System.Windows.Forms.CheckBox()
        Me.SENSOR_RESULTS = New System.Windows.Forms.Label()
        Me.CheckBox_SENSOR = New System.Windows.Forms.CheckBox()
        Me.COLOR_RESULTS = New System.Windows.Forms.Label()
        Me.CheckBox_COLOR = New System.Windows.Forms.CheckBox()
        Me.TEST_RESULT = New System.Windows.Forms.Label()
        Me.SCAN_RESULTS = New System.Windows.Forms.Label()
        Me.REG_RESULTS = New System.Windows.Forms.Label()
        Me.INTENSITY_RESULTS = New System.Windows.Forms.Label()
        Me.PLC_RESULTS = New System.Windows.Forms.Label()
        Me.ECHO_RESULTS = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TextBox_ECHO_CNT = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.CheckBox_PRINT = New System.Windows.Forms.CheckBox()
        Me.CheckBox_SCAN = New System.Windows.Forms.CheckBox()
        Me.CheckBox_REG = New System.Windows.Forms.CheckBox()
        Me.TextBox_PLC_ADDR = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.CheckBox_LIGHT = New System.Windows.Forms.CheckBox()
        Me.CheckBox_AMP = New System.Windows.Forms.CheckBox()
        Me.CheckBox_ECHO = New System.Windows.Forms.CheckBox()
        Me.Button_RUN = New System.Windows.Forms.Button()
        Me.Button_EXIT = New System.Windows.Forms.Button()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RunToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EditToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SettingsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.GroupBox1.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.ENERGY_RESULTS)
        Me.GroupBox1.Controls.Add(Me.CheckBox_ENERGY)
        Me.GroupBox1.Controls.Add(Me.SENSOR_RESULTS)
        Me.GroupBox1.Controls.Add(Me.CheckBox_SENSOR)
        Me.GroupBox1.Controls.Add(Me.COLOR_RESULTS)
        Me.GroupBox1.Controls.Add(Me.CheckBox_COLOR)
        Me.GroupBox1.Controls.Add(Me.TEST_RESULT)
        Me.GroupBox1.Controls.Add(Me.SCAN_RESULTS)
        Me.GroupBox1.Controls.Add(Me.REG_RESULTS)
        Me.GroupBox1.Controls.Add(Me.INTENSITY_RESULTS)
        Me.GroupBox1.Controls.Add(Me.PLC_RESULTS)
        Me.GroupBox1.Controls.Add(Me.ECHO_RESULTS)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.TextBox_ECHO_CNT)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.CheckBox_PRINT)
        Me.GroupBox1.Controls.Add(Me.CheckBox_SCAN)
        Me.GroupBox1.Controls.Add(Me.CheckBox_REG)
        Me.GroupBox1.Controls.Add(Me.TextBox_PLC_ADDR)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.CheckBox_LIGHT)
        Me.GroupBox1.Controls.Add(Me.CheckBox_AMP)
        Me.GroupBox1.Controls.Add(Me.CheckBox_ECHO)
        Me.GroupBox1.Location = New System.Drawing.Point(26, 26)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(338, 344)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'ENERGY_RESULTS
        '
        Me.ENERGY_RESULTS.AutoSize = True
        Me.ENERGY_RESULTS.Location = New System.Drawing.Point(115, 202)
        Me.ENERGY_RESULTS.Name = "ENERGY_RESULTS"
        Me.ENERGY_RESULTS.Size = New System.Drawing.Size(0, 13)
        Me.ENERGY_RESULTS.TabIndex = 22
        '
        'CheckBox_ENERGY
        '
        Me.CheckBox_ENERGY.AutoSize = True
        Me.CheckBox_ENERGY.Location = New System.Drawing.Point(7, 201)
        Me.CheckBox_ENERGY.Name = "CheckBox_ENERGY"
        Me.CheckBox_ENERGY.Size = New System.Drawing.Size(59, 17)
        Me.CheckBox_ENERGY.TabIndex = 21
        Me.CheckBox_ENERGY.Text = "Energy"
        Me.CheckBox_ENERGY.UseVisualStyleBackColor = True
        '
        'SENSOR_RESULTS
        '
        Me.SENSOR_RESULTS.AutoSize = True
        Me.SENSOR_RESULTS.Location = New System.Drawing.Point(116, 178)
        Me.SENSOR_RESULTS.Name = "SENSOR_RESULTS"
        Me.SENSOR_RESULTS.Size = New System.Drawing.Size(0, 13)
        Me.SENSOR_RESULTS.TabIndex = 20
        '
        'CheckBox_SENSOR
        '
        Me.CheckBox_SENSOR.AutoSize = True
        Me.CheckBox_SENSOR.Location = New System.Drawing.Point(7, 178)
        Me.CheckBox_SENSOR.Name = "CheckBox_SENSOR"
        Me.CheckBox_SENSOR.Size = New System.Drawing.Size(85, 17)
        Me.CheckBox_SENSOR.TabIndex = 19
        Me.CheckBox_SENSOR.Text = "Light Sensor"
        Me.CheckBox_SENSOR.UseVisualStyleBackColor = True
        '
        'COLOR_RESULTS
        '
        Me.COLOR_RESULTS.AutoSize = True
        Me.COLOR_RESULTS.Location = New System.Drawing.Point(115, 155)
        Me.COLOR_RESULTS.Name = "COLOR_RESULTS"
        Me.COLOR_RESULTS.Size = New System.Drawing.Size(0, 13)
        Me.COLOR_RESULTS.TabIndex = 18
        '
        'CheckBox_COLOR
        '
        Me.CheckBox_COLOR.AutoSize = True
        Me.CheckBox_COLOR.Location = New System.Drawing.Point(7, 155)
        Me.CheckBox_COLOR.Name = "CheckBox_COLOR"
        Me.CheckBox_COLOR.Size = New System.Drawing.Size(92, 17)
        Me.CheckBox_COLOR.TabIndex = 17
        Me.CheckBox_COLOR.Text = "Color Intensity"
        Me.CheckBox_COLOR.UseVisualStyleBackColor = True
        '
        'TEST_RESULT
        '
        Me.TEST_RESULT.AutoSize = True
        Me.TEST_RESULT.Font = New System.Drawing.Font("Microsoft Sans Serif", 40.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TEST_RESULT.Location = New System.Drawing.Point(148, 262)
        Me.TEST_RESULT.Name = "TEST_RESULT"
        Me.TEST_RESULT.Size = New System.Drawing.Size(0, 63)
        Me.TEST_RESULT.TabIndex = 16
        '
        'SCAN_RESULTS
        '
        Me.SCAN_RESULTS.AutoSize = True
        Me.SCAN_RESULTS.Location = New System.Drawing.Point(116, 247)
        Me.SCAN_RESULTS.Name = "SCAN_RESULTS"
        Me.SCAN_RESULTS.Size = New System.Drawing.Size(0, 13)
        Me.SCAN_RESULTS.TabIndex = 15
        '
        'REG_RESULTS
        '
        Me.REG_RESULTS.AutoSize = True
        Me.REG_RESULTS.Location = New System.Drawing.Point(116, 224)
        Me.REG_RESULTS.Name = "REG_RESULTS"
        Me.REG_RESULTS.Size = New System.Drawing.Size(0, 13)
        Me.REG_RESULTS.TabIndex = 14
        '
        'INTENSITY_RESULTS
        '
        Me.INTENSITY_RESULTS.AutoSize = True
        Me.INTENSITY_RESULTS.Location = New System.Drawing.Point(116, 132)
        Me.INTENSITY_RESULTS.Name = "INTENSITY_RESULTS"
        Me.INTENSITY_RESULTS.Size = New System.Drawing.Size(0, 13)
        Me.INTENSITY_RESULTS.TabIndex = 13
        '
        'PLC_RESULTS
        '
        Me.PLC_RESULTS.AutoSize = True
        Me.PLC_RESULTS.Location = New System.Drawing.Point(116, 108)
        Me.PLC_RESULTS.Name = "PLC_RESULTS"
        Me.PLC_RESULTS.Size = New System.Drawing.Size(0, 13)
        Me.PLC_RESULTS.TabIndex = 12
        '
        'ECHO_RESULTS
        '
        Me.ECHO_RESULTS.AutoSize = True
        Me.ECHO_RESULTS.Location = New System.Drawing.Point(116, 84)
        Me.ECHO_RESULTS.Name = "ECHO_RESULTS"
        Me.ECHO_RESULTS.Size = New System.Drawing.Size(0, 13)
        Me.ECHO_RESULTS.TabIndex = 11
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(271, 84)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(53, 13)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "Received"
        '
        'TextBox_ECHO_CNT
        '
        Me.TextBox_ECHO_CNT.Location = New System.Drawing.Point(206, 82)
        Me.TextBox_ECHO_CNT.Name = "TextBox_ECHO_CNT"
        Me.TextBox_ECHO_CNT.ReadOnly = True
        Me.TextBox_ECHO_CNT.Size = New System.Drawing.Size(64, 20)
        Me.TextBox_ECHO_CNT.TabIndex = 9
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(113, 46)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(50, 13)
        Me.Label2.TabIndex = 8
        Me.Label2.Text = "RESULT"
        '
        'CheckBox_PRINT
        '
        Me.CheckBox_PRINT.AutoSize = True
        Me.CheckBox_PRINT.Location = New System.Drawing.Point(7, 313)
        Me.CheckBox_PRINT.Name = "CheckBox_PRINT"
        Me.CheckBox_PRINT.Size = New System.Drawing.Size(99, 17)
        Me.CheckBox_PRINT.TabIndex = 7
        Me.CheckBox_PRINT.Text = "Print PLC Label"
        Me.CheckBox_PRINT.UseVisualStyleBackColor = True
        '
        'CheckBox_SCAN
        '
        Me.CheckBox_SCAN.AutoSize = True
        Me.CheckBox_SCAN.Location = New System.Drawing.Point(7, 247)
        Me.CheckBox_SCAN.Name = "CheckBox_SCAN"
        Me.CheckBox_SCAN.Size = New System.Drawing.Size(103, 17)
        Me.CheckBox_SCAN.TabIndex = 6
        Me.CheckBox_SCAN.Text = "Scan PLC Label"
        Me.CheckBox_SCAN.UseVisualStyleBackColor = True
        '
        'CheckBox_REG
        '
        Me.CheckBox_REG.AutoSize = True
        Me.CheckBox_REG.Location = New System.Drawing.Point(7, 224)
        Me.CheckBox_REG.Name = "CheckBox_REG"
        Me.CheckBox_REG.Size = New System.Drawing.Size(107, 17)
        Me.CheckBox_REG.TabIndex = 5
        Me.CheckBox_REG.Text = "Set Reg Defaults"
        Me.CheckBox_REG.UseVisualStyleBackColor = True
        '
        'TextBox_PLC_ADDR
        '
        Me.TextBox_PLC_ADDR.Location = New System.Drawing.Point(206, 17)
        Me.TextBox_PLC_ADDR.Name = "TextBox_PLC_ADDR"
        Me.TextBox_PLC_ADDR.ReadOnly = True
        Me.TextBox_PLC_ADDR.Size = New System.Drawing.Size(118, 20)
        Me.TextBox_PLC_ADDR.TabIndex = 4
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(113, 20)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(94, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "UUT PLC Address"
        '
        'CheckBox_LIGHT
        '
        Me.CheckBox_LIGHT.AutoSize = True
        Me.CheckBox_LIGHT.Location = New System.Drawing.Point(7, 132)
        Me.CheckBox_LIGHT.Name = "CheckBox_LIGHT"
        Me.CheckBox_LIGHT.Size = New System.Drawing.Size(91, 17)
        Me.CheckBox_LIGHT.TabIndex = 2
        Me.CheckBox_LIGHT.Text = "Light Intensity"
        Me.CheckBox_LIGHT.UseVisualStyleBackColor = True
        '
        'CheckBox_AMP
        '
        Me.CheckBox_AMP.AutoSize = True
        Me.CheckBox_AMP.Location = New System.Drawing.Point(7, 108)
        Me.CheckBox_AMP.Name = "CheckBox_AMP"
        Me.CheckBox_AMP.Size = New System.Drawing.Size(95, 17)
        Me.CheckBox_AMP.TabIndex = 1
        Me.CheckBox_AMP.Text = "PLC Amplitude"
        Me.CheckBox_AMP.UseVisualStyleBackColor = True
        '
        'CheckBox_ECHO
        '
        Me.CheckBox_ECHO.AutoSize = True
        Me.CheckBox_ECHO.Location = New System.Drawing.Point(7, 84)
        Me.CheckBox_ECHO.Name = "CheckBox_ECHO"
        Me.CheckBox_ECHO.Size = New System.Drawing.Size(51, 17)
        Me.CheckBox_ECHO.TabIndex = 0
        Me.CheckBox_ECHO.Text = "Echo"
        Me.CheckBox_ECHO.UseVisualStyleBackColor = True
        '
        'Button_RUN
        '
        Me.Button_RUN.Location = New System.Drawing.Point(33, 385)
        Me.Button_RUN.Name = "Button_RUN"
        Me.Button_RUN.Size = New System.Drawing.Size(75, 23)
        Me.Button_RUN.TabIndex = 1
        Me.Button_RUN.Text = "Run"
        Me.Button_RUN.UseVisualStyleBackColor = True
        '
        'Button_EXIT
        '
        Me.Button_EXIT.Location = New System.Drawing.Point(275, 385)
        Me.Button_EXIT.Name = "Button_EXIT"
        Me.Button_EXIT.Size = New System.Drawing.Size(75, 23)
        Me.Button_EXIT.TabIndex = 2
        Me.Button_EXIT.Text = "Exit"
        Me.Button_EXIT.UseVisualStyleBackColor = True
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.EditToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(395, 24)
        Me.MenuStrip1.TabIndex = 4
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RunToolStripMenuItem, Me.ToolStripMenuItem1, Me.ExitToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "File"
        '
        'RunToolStripMenuItem
        '
        Me.RunToolStripMenuItem.Name = "RunToolStripMenuItem"
        Me.RunToolStripMenuItem.Size = New System.Drawing.Size(98, 22)
        Me.RunToolStripMenuItem.Text = "Run"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(98, 22)
        Me.ExitToolStripMenuItem.Text = "Exit"
        '
        'EditToolStripMenuItem
        '
        Me.EditToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SettingsToolStripMenuItem})
        Me.EditToolStripMenuItem.Name = "EditToolStripMenuItem"
        Me.EditToolStripMenuItem.Size = New System.Drawing.Size(39, 20)
        Me.EditToolStripMenuItem.Text = "Edit"
        '
        'SettingsToolStripMenuItem
        '
        Me.SettingsToolStripMenuItem.Name = "SettingsToolStripMenuItem"
        Me.SettingsToolStripMenuItem.Size = New System.Drawing.Size(125, 22)
        Me.SettingsToolStripMenuItem.Text = "Settings..."
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(195, 385)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 5
        Me.Button2.Text = "New UUT"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(152, 22)
        Me.ToolStripMenuItem1.Text = "New UUT"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(395, 425)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button_EXIT)
        Me.Controls.Add(Me.Button_RUN)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "Form1"
        Me.Text = "Tekpea Manufacturing Test"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents TextBox_PLC_ADDR As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents CheckBox_LIGHT As CheckBox
    Friend WithEvents CheckBox_AMP As CheckBox
    Friend WithEvents CheckBox_ECHO As CheckBox
    Friend WithEvents Button_RUN As Button
    Friend WithEvents Button_EXIT As Button
    Friend WithEvents Label3 As Label
    Friend WithEvents TextBox_ECHO_CNT As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents CheckBox_PRINT As CheckBox
    Friend WithEvents CheckBox_SCAN As CheckBox
    Friend WithEvents CheckBox_REG As CheckBox
    Friend WithEvents SCAN_RESULTS As Label
    Friend WithEvents REG_RESULTS As Label
    Friend WithEvents INTENSITY_RESULTS As Label
    Friend WithEvents PLC_RESULTS As Label
    Friend WithEvents ECHO_RESULTS As Label
    Friend WithEvents TEST_RESULT As Label
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents FileToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents EditToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RunToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents SettingsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents Button2 As Button
    Friend WithEvents COLOR_RESULTS As Label
    Friend WithEvents CheckBox_COLOR As CheckBox
    Friend WithEvents SENSOR_RESULTS As Label
    Friend WithEvents CheckBox_SENSOR As CheckBox
    Friend WithEvents ENERGY_RESULTS As Label
    Friend WithEvents CheckBox_ENERGY As CheckBox
    Friend WithEvents ToolStripMenuItem1 As ToolStripMenuItem
End Class
