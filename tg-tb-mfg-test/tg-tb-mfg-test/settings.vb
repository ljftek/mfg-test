﻿Public Class settings
    Private Sub settings_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        textBox_NUM_ECHO_TEST.Text = My.Settings.TMT_ECHO_TEST_CNT.ToString()
        textBox_ECHO_PASS_NUM.Text = My.Settings.TMT_ECHO_TEST_PASS_CNT.ToString()
        textBox_AMP_TEST_PASS_VAL.Text = My.Settings.TMT_AMP_TEST_ACCEPT.ToString()

        TextBox_UUT_TX_GAIN.Text = My.Settings.TMT_UUT_TX_GAIN.ToString()
        TextBox_BRG_TX_GAIN.Text = My.Settings.TMT_BRG_TX_GAIN.ToString()

        TextBox_TRIGGER_VOLTAGE.Text = My.Settings.TMT_TRIGGER_VOLTAGE.ToString()
        TextBox_VISA_ID.Text = My.Settings.TMT_VISA_ID.ToString()




        textBox_LOG_ADDRESS.Text = My.Settings.TMT_LOGICAL_ADDRESS.ToString()

        TextBox_COM_PORT.Text = My.Settings.TMT_COM_PORT
        TextBox_LOGFILE.Text = My.Settings.TMT_LOGFILE




    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Close()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        DialogResult = DialogResult.OK
        ' This Is a sad way to pass data between two forms
        ' duplicating code to save the settings:( Ack-Ugh
        My.Settings.TMT_AMP_TEST_ACCEPT = Convert.ToDouble(textBox_AMP_TEST_PASS_VAL.Text)

        My.Settings.TMT_ECHO_TEST_CNT = Convert.ToInt32(textBox_NUM_ECHO_TEST.Text)
        My.Settings.TMT_ECHO_TEST_PASS_CNT = Convert.ToInt32(textBox_ECHO_PASS_NUM.Text)



        My.Settings.TMT_LOGICAL_ADDRESS = Convert.ToInt32(textBox_LOG_ADDRESS.Text)

        My.Settings.TMT_COM_PORT = TextBox_COM_PORT.Text
        My.Settings.TMT_LOGFILE = TextBox_LOGFILE.Text

        My.Settings.TMT_BRG_TX_GAIN = Convert.ToInt32(TextBox_BRG_TX_GAIN.Text)
        My.Settings.TMT_UUT_TX_GAIN = Convert.ToInt32(TextBox_UUT_TX_GAIN.Text)

        My.Settings.TMT_TRIGGER_VOLTAGE = Convert.ToDouble(TextBox_TRIGGER_VOLTAGE.Text)
        My.Settings.TMT_VISA_ID = TextBox_VISA_ID.Text

        My.Settings.Save() ' // save the configurable parameters
        My.Settings.Upgrade()
        Close()

    End Sub


End Class