﻿Imports System.IO
Imports NationalInstruments.Visa
'
' This is a mfg test for TG-TB gateway/bridge

' to use scope:
' was using extenions references for 
'  nationalinstruments visa
' ividriver
' ivi visa assembly


Public Class Form1
    Const DEFAULT_TMT_TEST_VERSION_MAJOR = 0 ' // major version number
    Const DEFAULT_TMT_TEST_VERSION_MINOR = 6 ' // minor version number.  Change this when you add app setting

    'Dim rs485_RESPONSE As String = "01050001ff00ddfa" ' magic string that comes back from mbrequest
    Dim rs485_RESPONSE_ON = New Byte() {1, 1, 1, 1, 90, 48} ' magic string that comes back from mbrequest
    Dim rs485_RESPONSE_OFF = New Byte() {1, 1, 1, 0, 51, 88} ' magic string that comes back from mbrequest
    Dim rs485_MBR_RAW = New Byte() {127, 127, 253, 255, 253, 167, 214, 0, 0} ' magic string that comes back from mbrequest
    Dim rs485_MBR_LEN As Integer = 8
    Dim rs485_RESPONSE_LEN As Integer = 6
    Dim comPort As IO.Ports.SerialPort = Nothing

    Dim brinePort As String = "--tty /dev/ttyATH0"
    'Dim brinePort As String = "--tcp 192.168.70.3"

    Dim USE_SCOPE As Boolean = True
    'Dim USE_SCOPE As Boolean = False

    Const DEFAULT_LOGFILE = "c:\mfg-test\TG-TB_MFG_LOG.txt"

    Const DEFAULT_NUM_ECHO_TESTS = 5 ' // how many echos For an echo test
    Const DEFAULT_NUM_ECHO_TEST_PASS_CNT = 5
    Const DEFAULT_AMP_TEST_ACCEPT_VALUE = 3.2


    Const DEFAULT_TRIGGER_VOLTAGE = 2
    Const DEFAULT_VISA_ID = "USB0::0x1AB1::0x0588::DS1ET161051698::INSTR" '  //Get VISA resource ID 

    Const DEFAULT_BRG_TX_GAIN = &HC
    Const DEFAULT_UUT_TX_GAIN = &HC

    'Const DEFAULT_LOGICAL_ADDRESS = 1
    Const DEFAULT_LOGICAL_ADDRESS = 16 ' with a gateway, the UUT gets 16
    ' rs485=12, my usbserial=4
    Const DEFAULT_COM_PORT = "COM12"
    'Const RS485_SPEED = 4800 ' 4-20ma
    Const RS485_SPEED = 9600 ' inepro meter, bridge 485
    Dim USE_RS485 As Boolean = True

    Const PROCESS_TIMEOUT = 10000
    'Const PLC_ADDR_POS = 12  ' index within response str Of PLC address.  for PC
    Const PLC_ADDR_POS = 10  ' index within response str Of PLC address.  for gateway!!!
    Const SOCK_VAL_POS = 15 'index within response str of socket value
    Const CMD_DELAY = 2000 ' ms between commands to putty
    Const SCOPE_DELAY = 300 ' ms between scope commands

    ' scope related
    Dim mbSession As MessageBasedSession
    Dim strVISARsrc As String = "USB0::0x1AB1::0x0588::DS1ET161051698::INSTR" '  //Get VISA resource ID 
    Dim triggerVoltage As Double

    ' defaults for the configurable parameters

    Dim testVersionMajor As Integer = DEFAULT_TMT_TEST_VERSION_MAJOR
    Dim testVersionMinor As Integer = DEFAULT_TMT_TEST_VERSION_MINOR


    Dim UUTTxGain As Integer
    Dim BRGTXGain As Integer
    Dim ampTestAccept As Double
    Dim echoTestCnt As Integer
    Dim echoTestPassCnt As Integer

    Dim RS485_COMPORT As String  ' com port for talking rs485 to LG3100
    Dim rs485Resp As Boolean
    Dim plcAddr As String
    Dim TG485PlcAddr As String
    Dim addr As String

    Dim energy As Integer
    Dim meterReading As Double
    Dim tokCount As Integer = 0
    Dim linTok As Int32

    Dim echoRcv As Integer = 0

    Dim logAddr As Integer '  the logical address To use
    Dim logFile As String = DEFAULT_LOGFILE ' file to save to
    Dim putty As New Process()
    Dim myStreamWriter As StreamWriter


    Dim PASS_STR As String = "PASS"
    Dim FAIL_STR As String = "FAIL"
    Private Sub getTG485_PLCAddr()
        runCmd(CMD_GET_TG485_ADDR, "")
        If (addr = "") Then
            MessageBox.Show("Unable to retrieve TG485 PLC address.")
            TG485PlcAddr = " "
        Else
            TG485PlcAddr = addr
        End If

        TextBox_TG485_ADDR.Text = TG485PlcAddr
        TextBox_TG485_ADDR.BackColor = Color.White
    End Sub
    Private Sub getPLCAddr()
        runCmd(CMD_GET_ADDR, "")
        If (addr = "") Then
            MessageBox.Show("Unable to retrieve PLC address.")
            plcAddr = " "
        Else
            plcAddr = addr
        End If

        TextBox_PLC_ADDR.Text = plcAddr
        TextBox_PLC_ADDR.BackColor = Color.White
    End Sub

    Private Sub newUUT()
        ' new uut
        addr = ""
        plcAddr = ""
        TG485PlcAddr = ""
        getTG485_PLCAddr()
        Threading.Thread.Sleep(2000)
        getPLCAddr()
        'runCmd(CMD_LIGHT, "0 0 0") ' turn off the light

        clearAllResults()
    End Sub

    Private Sub createSession()
        Dim ptyIPAddr As String = "192.168.70.1"
        'Dim ptyIPAddr As String = "10.1.11.43"
        Dim tok As Integer
        ' this routine logs in and runs brine
        'Me.Cursor = Cursors.WaitCursor

        putty.StartInfo.FileName = "C:\PuTTY\plink.exe"


        AddHandler putty.OutputDataReceived, AddressOf OutputHandler

        putty.StartInfo.Arguments = " " & ptyIPAddr

        putty.StartInfo.UseShellExecute = False
        putty.StartInfo.RedirectStandardOutput = True
        putty.StartInfo.RedirectStandardError = True
        putty.StartInfo.RedirectStandardInput = True
        putty.StartInfo.CreateNoWindow = True
        putty.Start()
        myStreamWriter = putty.StandardInput
        putty.BeginOutputReadLine()
        'putty.BeginErrorReadLine()

        'Threading.Thread.Sleep(6000)

        'writeScope(":run")
        'Threading.Thread.Sleep(300)
        tok = sesWait(5, uutToks.login)
        myStreamWriter.WriteLine("root")
        '        Threading.Thread.Sleep(5000)
        tok = sesWait(5, uutToks.password)
        myStreamWriter.WriteLine("ar1kan")
        Threading.Thread.Sleep(5000)
        'tok = sesWait(5)
        myStreamWriter.WriteLine("/etc/init.d/tkpgateway stop")
        Threading.Thread.Sleep(CMD_DELAY)
        'tok = sesWait(1)
        myStreamWriter.WriteLine(" /opt/tekpea/bin/brine -v " + brinePort)
        'Threading.Thread.Sleep(CMD_DELAY)
        tok = sesWait(1, uutToks.brgCnct)
        ' set log addr 1 and turn off idents
        myStreamWriter.WriteLine("addr log " + logAddr.ToString())
        'Threading.Thread.Sleep(CMD_DELAY)
        tok = sesWait(1, uutToks.brgCnct)
        myStreamWriter.WriteLine("setclock") ' stop idents
        'Threading.Thread.Sleep(CMD_DELAY)
        tok = sesWait(1, uutToks.brgCnct)
        'myStreamWriter.WriteLine("cfgwrite 1024 00") ' turn off automatic light control
        'Threading.Thread.Sleep(CMD_DELAY)
    End Sub

    Private Sub closeSession()
        myStreamWriter.WriteLine("exit") ' exit from brine
        Threading.Thread.Sleep(CMD_DELAY)
        myStreamWriter.WriteLine("exit") ' exit from gateway putty
        Threading.Thread.Sleep(CMD_DELAY)
        putty.WaitForExit(PROCESS_TIMEOUT) ' 10 second timeout

        putty.CancelOutputRead()
        RemoveHandler putty.OutputDataReceived, AddressOf OutputHandler

        putty.Close()
    End Sub


    Private Sub runCmd(ByVal cmdMask As Integer, param As String)
        Me.Cursor = Cursors.WaitCursor

        writeScope(":run")
        Threading.Thread.Sleep(SCOPE_DELAY)

        If (cmdMask = CMD_GET_ADDR) Then
            myStreamWriter.WriteLine("cfgread 6 8")
            sesWait(5, uutToks.RESPONSE)
            Threading.Thread.Sleep(CMD_DELAY)
        End If

        If (cmdMask = CMD_GET_TG485_ADDR) Then
            myStreamWriter.WriteLine("brread 6 8")
            'Threading.Thread.Sleep(CMD_DELAY)
            sesWait(5, uutToks.RESPONSE)
        End If

        If (cmdMask = CMD_ECHO) Then
            Dim gain As String
            gain = UUTTxGain.ToString("X2")
            myStreamWriter.WriteLine("brgwrite 0106 " + gain)
            'Threading.Thread.Sleep(CMD_DELAY)
            For i As Integer = 1 To echoTestCnt
                myStreamWriter.WriteLine("brecho 0101")
                Threading.Thread.Sleep(CMD_DELAY)
                'sesWait(5, uutToks.ECHO_OK)
            Next
            'myStreamWriter.WriteLine("brgwrite 0106 0E")
            'Threading.Thread.Sleep(CMD_DELAY)
        End If

        If (cmdMask = CMD_AMP) Then
            Dim gain As String
            gain = BRGTXGain.ToString("X2")
            'myStreamWriter.WriteLine("brwrite 0106 " + gain) ' set bridge txgain to min
            myStreamWriter.WriteLine("cfgwrite 0106 11") ' set UUT txgain to min
            myStreamWriter.WriteLine("brwrite 0106 EE") ' set bridge txgain to max - THIS TG/TB bridge requires the EE, not 0E
            'Threading.Thread.Sleep(CMD_DELAY)

            myStreamWriter.WriteLine("echo 0101")
            Threading.Thread.Sleep(CMD_DELAY)
            myStreamWriter.WriteLine("cfgwrite 0106 EE") ' set UUT txgain to min
            'myStreamWriter.WriteLine("brwrite 0106 0E") ' set bridge txgain to max
            Threading.Thread.Sleep(CMD_DELAY)
        End If

        If (cmdMask = CMD_RS_ECHO) Then
            'myStreamWriter.WriteLine("mbrequest 8 01050001ff00DDFA 0x3e8") ' sends packet out over bridge modbus
            myStreamWriter.WriteLine("mbrequest 6 010100010001 0x3e8") ' sends packet out over bridge modbus

            'Threading.Thread.Sleep(CMD_DELAY)
        End If

        If (cmdMask = CMD_REG) Then
            myStreamWriter.WriteLine("cfgwrite 0106 EE") ' set tx gain to default
            Threading.Thread.Sleep(CMD_DELAY)

            myStreamWriter.WriteLine("cfgwrite 0107 07") ' set rx gain to default
            Threading.Thread.Sleep(CMD_DELAY)
            myStreamWriter.WriteLine("cfgwrite 100E 01") ' set ident period to default
            Threading.Thread.Sleep(CMD_DELAY)
        End If


        Me.Cursor = Cursors.Default

    End Sub

    Dim UUTTokens() As String = {"Ready", "COMM_NORESP", "PHY", "ECHO SUCCESS", "Response:", "Success", "timeout", "Socket value:", "login as:", "password:", "[Bridge connected]"}
    Public Enum uutToks
        READY
        NORESP
        PHY
        ECHO_OK
        RESPONSE
        SUCCESS
        TIMEOUT
        SOCKET_VAL
        login
        password
        brgCnct
        UUT_NUM_TOKS
    End Enum

    Const CMD_GET_ADDR = &H1
    Const CMD_ECHO = &H2
    Const CMD_AMP = &H4
    Const CMD_LIGHT = &H8
    Const CMD_REG = &H10
    Const CMD_SCAN = &H20
    Const CMD_PRINT = &H40
    Const CMD_COLOR = &H80
    Const CMD_SENSOR = &H100
    Const CMD_ENERGY = &H200
    Const CMD_RS_ECHO = &H400
    Const CMD_GET_TG485_ADDR = &H800


    Private Function parseUUT(ByVal bridgeResp As String)

        For i As Integer = 0 To uutToks.UUT_NUM_TOKS - 1
            Dim b As Boolean = bridgeResp.Contains(UUTTokens(i))
            If (b) Then
                Return i
            End If
        Next

        Return -1
    End Function

    Function sesWait(ByVal timeout As Integer, tok As Integer)
        ' wait for a specific token or timeout
        Dim iTok As Integer

        iTok = tokCount
        While ((tokCount = iTok And linTok <> tok) And timeout <> 0)
            Threading.Thread.Sleep(1000)
            timeout -= 1
        End While
        If (iTok <> tokCount) Then
            Return linTok
        End If
        Return -1 ' timeout
    End Function

    Sub OutputHandler(sender As Object, e As DataReceivedEventArgs)
        If Not String.IsNullOrEmpty(e.Data) Then
            tokCount += 1

            ' Add the text to the collected output.
            Console.WriteLine(e.Data)
            linTok = parseUUT(e.Data)
            Console.WriteLine(linTok)
            Select Case linTok
                Case uutToks.READY
                Case uutToks.NORESP
                Case uutToks.PHY
                Case uutToks.ECHO_OK
                    echoRcv = echoRcv + 1
                Case uutToks.RESPONSE
                    ' PLC addr - format Is Response 16hexchars
                    addr = e.Data.Substring(PLC_ADDR_POS)
                    'If (addr <> rs485_RESPONSE_ON And addr <> rs485_RESPONSE_OFF) Then

                    addr = addr.ToUpper()
                    'Else
            'rs485Resp = True
        'End If

                Case uutToks.SOCKET_VAL
                    Dim energyStr As String = e.Data.Substring(SOCK_VAL_POS)
                    energy = Convert.ToInt32(energyStr)
                Case uutToks.SUCCESS
                Case uutToks.TIMEOUT
                    Console.WriteLine(e.Data)
                Case uutToks.login
                    Console.WriteLine(e.Data)
                Case uutToks.password
                    Console.WriteLine(e.Data)
            End Select
        End If

    End Sub

    Sub setResults(ByVal Label As Label, value As Integer)
        ' set the label to pass/fail based on value
        ' set the back color to green/red
        If (value = 1) Then
            ' passed
            Label.BackColor = Color.Lime
            Label.Text = PASS_STR
        ElseIf (value = 0) Then
            ' Clear it
            Label.Text = ""

        ElseIf (value = -1) Then
            ' failed
            Label.BackColor = Color.Red
            Label.Text = FAIL_STR
        End If
        'Threading.Thread.Sleep(1000) ' give UI window processing time
        'System.Windows.Forms.Application.DoEvents()
        Me.Refresh()
    End Sub
    Private Sub clearAllResults()
        echoRcv = 0

        TextBox_ECHO_CNT.Text = ""
        setResults(ECHO_RESULTS, 0)
        setResults(PLC_RESULTS, 0)
        setResults(REG_RESULTS, 0)
        setResults(SCAN_RESULTS, 0)
        setResults(RS_ECHO_RESULTS, 0)

        setResults(TEST_RESULT, 0) ' the absolute pass/fail
    End Sub


    '--------------------------------------------------
    ' application entry point
    '--------------------------------------------------
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        loadAppSettings()
        initScope()
        createSession()
        openRS485()
        newUUT()

    End Sub
    '--------------------------------------------------
    ' application exit point
    '--------------------------------------------------

    Private Sub main_master_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        saveAppSettings()
        closeSession()
        closeScope()
        closeRS485()
    End Sub
    Private Sub readScope(ByRef parm As String)
        If (USE_SCOPE) Then
            parm = mbSession.RawIO.ReadString()
        Else
            parm = "-99"
        End If
    End Sub

    Private Sub writeScope(ByVal parm As String)
        If (USE_SCOPE) Then
            mbSession.RawIO.Write(parm)
        End If

    End Sub


    Private Sub initScope()
        If USE_SCOPE Then

            Try

                mbSession = New UsbSession(strVISARsrc, Ivi.Visa.AccessModes.LoadConfig, 10000, True) 'Instantiate And open a Message Based VISA Session
                mbSession.TerminationCharacterEnabled = True ' Important
                Dim textToWrite As String = ""
                Dim strRead As String

                writeScope("*idn?")
                readScope(strRead)


                writeScope(":CHANnel1:Display On")
                Threading.Thread.Sleep(300)
                writeScope(":CHANnel2:Display On")
                Threading.Thread.Sleep(300)
                ' set up channel 1

                Threading.Thread.Sleep(300)
                writeScope(":Channel1:BWlimit off")
                Threading.Thread.Sleep(300)
                writeScope(":channel1:probe 1") ' 1x
                Threading.Thread.Sleep(300)
                writeScope(":TRIGGER:EDGE:SOURCE CHANnel1")
                Threading.Thread.Sleep(300)
                writeScope(":TRIGGER:EDGE:Sweep Normal")
                Threading.Thread.Sleep(300)
                'writeScope(":TRIGGER:EDGE:level 2")
                writeScope(":TRIGGER:EDGE:level " + triggerVoltage.ToString())

                Threading.Thread.Sleep(300)
                writeScope(":channel1:offset 0")
                Threading.Thread.Sleep(300)
                'writeScope(":channel1:scale 5")
                Threading.Thread.Sleep(300)

                Threading.Thread.Sleep(300)
                writeScope(":timebase:offset 0")
                Threading.Thread.Sleep(300)
                writeScope(":TIMebase:SCALe 0.05")
                Threading.Thread.Sleep(300)

                ' set up channel 2

                writeScope(":TRIGGER:EDGE:Coupling DC")
                Threading.Thread.Sleep(300)
                writeScope(":TRIGger:EDGE:SWEep AUTO CHANnel2")
                Threading.Thread.Sleep(300)
                writeScope(":measure:vaverage? chan2")
                Threading.Thread.Sleep(300)
                writeScope(":channel2:offset -4")
                Threading.Thread.Sleep(300)
                writeScope(":channel2:scale 2")
                Threading.Thread.Sleep(300)
                writeScope(":channel2:probe 10")

                Threading.Thread.Sleep(300)

                writeScope(":measure:clear")
                Threading.Thread.Sleep(300)

                writeScope(":run")

            Catch ex As Ivi.Visa.NativeVisaException
                MessageBox.Show("Unable to connect to oscilloscope.  Is it plugged into the USB port?")
                USE_SCOPE = False

            End Try
        End If

    End Sub
    Private Sub closeScope()
        Try

            ' close the scope session

            writeScope(":key:forc")
            mbSession.Clear()
            mbSession.Dispose() 'shut it down
        Catch
        End Try

    End Sub

    ' modbus
    Sub readPwrMeter()
        Dim comPort As IO.Ports.SerialPort = Nothing
        'Dim buf As Byte() = New Byte() {0, 3, &H20, &H80, 0, 2, &HCF, &HF2} ' power , broadcast addr adapter  CF F2 - inepro
        'Dim buf As Byte() = New Byte() {0, 3, &H20, &H0, 0, 2, &HCE, &H1A} ' voltage, broadcast addr adapter  CF F2 - inepro
        Dim buf As Byte() = New Byte() {0, 3, 0, &H40, 0, 2, &HC5, &HDF} ' read t/h 4-20ma 01 03 00 40 00 02 c5 df

        Dim buf1(100) As Byte

        Dim readBytes As Integer
        Try

            comPort = My.Computer.Ports.OpenSerialPort(RS485_COMPORT, RS485_SPEED)
            comPort.Parity = IO.Ports.Parity.Even
            comPort.StopBits = IO.Ports.StopBits.One
            comPort.DataBits = 8
            comPort.Encoding = System.Text.Encoding.UTF8
            comPort.ReadTimeout = 1000 ' timeout on read
            comPort.Write(buf, 0, 8)
            ' delay a bit and read
            Threading.Thread.Sleep(200)

            readBytes = comPort.Read(buf1, 0, 10)
            If (readBytes = 9) Then
                ' need to reverse the order of the bytes
                buf(3) = buf1(3)
                buf(2) = buf1(4)
                buf(1) = buf1(5)
                buf(0) = buf1(6)
                meterReading = BitConverter.ToSingle(buf, 0)
            End If

        Catch ex As TimeoutException
        Finally
            If comPort IsNot Nothing Then comPort.Close()
        End Try

    End Sub
    Sub read4_20()
        Dim comPort As IO.Ports.SerialPort = Nothing
        'Dim buf As Byte() = New Byte() {0, 3, &H20, &H80, 0, 2, &HCF, &HF2} ' power , broadcast addr adapter  CF F2 - inepro
        'Dim buf As Byte() = New Byte() {0, 3, &H20, &H0, 0, 2, &HCE, &H1A} ' voltage, broadcast addr adapter  CF F2 - inepro
        Dim buf As Byte() = New Byte() {1, 3, 0, &H40, 0, 2, &HC5, &HDF} ' read t/h 4-20ma 01 03 00 40 00 02 c5 df

        Dim buf1(100) As Byte
        Dim readBytes As Integer = 0

        Try
            comPort = My.Computer.Ports.OpenSerialPort(RS485_COMPORT, RS485_SPEED)
            comPort.Parity = IO.Ports.Parity.None
            comPort.StopBits = IO.Ports.StopBits.One
            comPort.DataBits = 8
            comPort.Encoding = System.Text.Encoding.UTF8
            comPort.ReadTimeout = 1000 ' timeout on read
            'comPort.Write(buf, 0, 8)
            ' delay a bit and read
            Threading.Thread.Sleep(200)


        Catch ex As TimeoutException
        End Try
        While (readBytes <> 10)
            Try
                readBytes = comPort.Read(buf1, 0, 11)
                If (readBytes > 0) Then
                    ' need to reverse the order of the bytes
                    buf(3) = buf1(3)
                    buf(2) = buf1(4)
                    buf(1) = buf1(5)
                    buf(0) = buf1(6)
                End If

            Catch ex As TimeoutException
            End Try

            Threading.Thread.Sleep(500)
        End While
        If comPort IsNot Nothing Then comPort.Close()

    End Sub
    Sub flushRS485()
        If (USE_RS485) Then
            comPort.DiscardInBuffer()
        End If
    End Sub

    Sub openRS485()
        ' open the RS485 com port and flush the buffer


        Try
            comPort = My.Computer.Ports.OpenSerialPort(RS485_COMPORT, RS485_SPEED)
            comPort.Parity = IO.Ports.Parity.None
            comPort.StopBits = IO.Ports.StopBits.One
            comPort.DataBits = 8
            comPort.Encoding = System.Text.Encoding.UTF8
            comPort.ReadTimeout = 1000 ' timeout on read

        Catch ex As System.IO.IOException
            MessageBox.Show("Unable to open RS485.  Is it plugged into the USB port?")
            USE_RS485 = False

        End Try

        flushRS485()

    End Sub

    Function read485()
        'Dim comPort As IO.Ports.SerialPort = Nothing
        Dim buf1(100) As Byte
        Dim readBytes As Integer = 0
        Dim ret As Integer = 1
        Dim i, j As Integer
        Dim readTime As Integer = 5

        If (USE_RS485 = False) Then
            Return 0
        End If
        While (readBytes < rs485_MBR_LEN And readTime <> 0)
            Try
                i = comPort.Read(buf1, 0, rs485_MBR_LEN)
                If (i > 0) Then
                    For j = 0 To i - 1
                        If (buf1(j) <> rs485_MBR_RAW(readBytes + j)) Then ' 
                            ret = -1
                        End If
                    Next j

                    readBytes += i
                    If (readBytes >= rs485_MBR_LEN) Then
                        readTime = 0 ' time to exit the loop
                    End If
                End If

            Catch ex As TimeoutException
            End Try
            If (readTime <> 0) Then
                Threading.Thread.Sleep(1000)
                readTime -= 1
            End If
        End While
        'If comPort IsNot Nothing Then comPort.Close()
        Return ret
    End Function
    Sub closeRS485()
        If comPort IsNot Nothing Then comPort.Close()

    End Sub

    ' log results
    Private Sub logResults(ByVal logLine As String)
        Dim path As String = "c:\mfg-test"

        Try
            ' Determine whether the directory exists.
            If (Directory.Exists(path) = False) Then
                ' Try to create the directory.
                Dim di As DirectoryInfo = Directory.CreateDirectory(path)
                Console.WriteLine("The directory was created successfully at {0}.", Directory.GetCreationTime(path))
            End If
        Catch e As Exception
            Console.WriteLine("The process failed: {0}.", e.ToString())
        End Try


        ' append a line to the logfile
        Dim objWriter As New System.IO.StreamWriter(logFile, True)

        objWriter.WriteLine(logLine)
        'My.Computer.FileSystem.WriteAllText(logFile, logLine, True)

        objWriter.Close()

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button_EXIT.Click

        Application.Exit()
    End Sub

    Private Sub Button_RUN_Click(sender As Object, e As EventArgs) Handles Button_RUN.Click
        Dim cmd As Integer = 0
        Dim testResults As Integer = 1 ' pass by default
        Dim f As Double
        Dim strRead As String

        echoRcv = 0

        TextBox_ECHO_CNT.Text = ""
        clearAllResults()

        Dim runTime As Date = Date.Now
        Dim logLine As String

        logLine = "Test Ver:" + testVersionMajor.ToString() + "." + testVersionMinor.ToString() + " Execution Date:" + runTime.ToShortDateString() + " Time: " + runTime.ToShortTimeString() + " DC-DC PLC Addr: " + plcAddr.ToString() + " TG485 Addr: " + TG485PlcAddr.ToString()
        logResults(logLine)
        If (USE_SCOPE = False) Then
            logLine = "No Scope"
            logResults(logLine)
        End If
        If (USE_RS485 = False) Then
            logLine = "No RS485"
            logResults(logLine)
        End If

        If (CheckBox_ECHO.Checked = True) Then
            cmd = CMD_ECHO
            writeScope(":CHANnel1:Display On")
            Threading.Thread.Sleep(300)
            writeScope(":CHANnel2:Display Off")
            Threading.Thread.Sleep(300)
            runCmd(cmd, "")


            TextBox_ECHO_CNT.Text = " " + echoRcv.ToString() + " out of " + echoTestCnt.ToString()
            If (echoTestCnt = echoRcv) Then
                logLine = "Echo Test: Pass - " + echoRcv.ToString() + " out of " + echoTestCnt.ToString()
                setResults(ECHO_RESULTS, 1)
            Else
                logLine = "Echo Test: Fail - " + echoRcv.ToString() + " out of " + echoTestCnt.ToString()
                setResults(ECHO_RESULTS, -1)
                testResults = -1
            End If
            logResults(logLine)
        End If

        If (CheckBox_AMP.Checked = True) Then
            cmd = CMD_AMP
            writeScope(":stop")
            Threading.Thread.Sleep(300)

            writeScope(":CHANnel1:Display On")
            Threading.Thread.Sleep(300)
            writeScope(":CHANnel2:Display Off")
            Threading.Thread.Sleep(300)
            writeScope(":TRIGGER:Edge:Sweep Single")
            Threading.Thread.Sleep(300)
            writeScope(":TRIGGER:EDGE:Coupling AC")
            writeScope(":measure:clear")
            Threading.Thread.Sleep(300)
            writeScope(":measure:clear chan1")
            Threading.Thread.Sleep(300)
            runCmd(cmd, "")
            writeScope(":trigger:status?")
            Threading.Thread.Sleep(300)
            readScope(strRead)
            'Query returns “RUN”, “STOP”, “T‟D”, “WAIT”, “SCAN” or “AUTO”. If we are not in STOP mode, we didnt trigger!
            If (strRead = "STOP") Then
                Threading.Thread.Sleep(300)
                writeScope(":measure:vpp? chan1")

                readScope(strRead)
            Else
                strRead = "0"
            End If


            ' when we read the value from the scope, we get a string in scientific notation
            ' ie: 99e36
            f = Double.Parse(strRead, System.Globalization.NumberStyles.Float)
            If (f < ampTestAccept) Then
                testResults = -1
                setResults(PLC_RESULTS, -1)
                logLine = "Amp Test: Fail - " + strRead
            Else
                setResults(PLC_RESULTS, 1)
                logLine = "Amp Test: Pass - " + strRead
            End If

            logResults(logLine)
            writeScope(":TRIGGER:Edge:Sweep Single")

        End If

        If (CheckBox_RS_ECHO.Checked = True) Then
            cmd = CMD_RS_ECHO
            'rs485Resp = False ' default to fail
            flushRS485()

            runCmd(cmd, "")
            Dim ret As Integer
            ret = read485()

            If (ret = 1) Then
                logLine = "RS485 Echo Test: Pass"
                setResults(RS_ECHO_RESULTS, 1)
            Else
                logLine = "RS485 Echo Test: Fail - "
                setResults(RS_ECHO_RESULTS, -1)
                testResults = -1
            End If
            logResults(logLine)
        End If


        If (CheckBox_REG.Checked = True) Then
            cmd = CMD_REG
            runCmd(cmd, "")
        End If



        If (CheckBox_SCAN.Checked = True) Then

            cmd = CMD_SCAN
            Dim scanPLC As String = dlgEntry("TG485 PLC Address")

            If (scanPLC.Equals(TG485PlcAddr)) Then
                setResults(SCAN_RESULTS, 1)
                logLine = "Scan Test: pass PLC addr: " + TG485PlcAddr.ToString() + " Scanned Addr: " + scanPLC.ToString()

            Else
                logLine = "Scan Test: fail PLC addr: " + TG485PlcAddr.ToString() + " Scanned Addr: " + scanPLC.ToString()

                setResults(SCAN_RESULTS, -1)
                testResults = -1
            End If
            logResults(logLine)
        End If



        If (CheckBox_PRINT.Checked = True) Then
            cmd = CMD_PRINT
        End If

        setResults(TEST_RESULT, testResults)


    End Sub


    Function dlgEntry(ByVal caption As String)
        Dim ret As String
        Dim prompt As New Form
        prompt.Width = 200
        prompt.Height = 150
        prompt.Text = ""
        Dim Textbox1 As New TextBox()
        Dim Button1 As New Button()

        Dim lb As New Label
        lb.Text = caption
        lb.Location = New Point(20, 20)


        'TEXTBOX PROPERTIES

        Textbox1.Location = New System.Drawing.Point(20, 50)
        Textbox1.Name = "TextBox1"
        Textbox1.Size = New System.Drawing.Size(140, 20)
        Textbox1.TabIndex = 1
        Textbox1.Show()


        'BUTTON PROPERTIES
        Button1.Location = New System.Drawing.Point(30, 80)
        Button1.Name = "Button1"
        Button1.Size = New System.Drawing.Size(120, 20)
        Button1.TabIndex = 2
        Button1.Text = "OK"
        Button1.DialogResult = DialogResult.OK
        'AddHandler Button1.Click, AddressOf Me.btnClick

        prompt.Controls.Add(lb)
        prompt.Controls.Add(Button1)
        prompt.Controls.Add(Textbox1)

        ' Show testDialog as a modal dialog

        prompt.ShowDialog(Me)

        ret = Textbox1.Text
        prompt.Close()
        prompt.Dispose()

        Return ret.ToUpper()

    End Function

    Private Sub Button2_Click_1(sender As Object, e As EventArgs) Handles Button2.Click
        Me.Cursor = Cursors.WaitCursor
        closeSession()
        createSession()
        newUUT()
        Me.Cursor = Cursors.Default
    End Sub
    Public Sub saveAppSettings()

        ' load the configurable parameters

        My.Settings.TMT_TEST_VERSION_MAJOR = testVersionMajor
        My.Settings.TMT_TEST_VERSION_MINOR = testVersionMinor
        My.Settings.TMT_AMP_TEST_ACCEPT = ampTestAccept
        My.Settings.TMT_ECHO_TEST_CNT = echoTestCnt
        My.Settings.TMT_ECHO_TEST_PASS_CNT = echoTestPassCnt


        My.Settings.TMT_LOGICAL_ADDRESS = logAddr

        My.Settings.TMT_COM_PORT = RS485_COMPORT
        My.Settings.TMT_LOGFILE = logFile

        My.Settings.TMT_TRIGGER_VOLTAGE = triggerVoltage
        My.Settings.TMT_VISA_ID = strVISARsrc

        My.Settings.TMT_UUT_TX_GAIN = UUTTxGain
        My.Settings.TMT_BRG_TX_GAIN = BRGTXGain

        My.Settings.Save() ' // save the configurable parameters
        My.Settings.Upgrade()
    End Sub

    Private Sub loadAppSettings()

        ' load the configurable parameters

        Dim diskVersionMajor As Integer = My.Settings.TMT_TEST_VERSION_MAJOR
        Dim diskVersionMinor As Integer = My.Settings.TMT_TEST_VERSION_MINOR
        '// what are the settings from disk?  If this Is a different version, load defaults
        If (diskVersionMajor <> testVersionMajor Or diskVersionMinor <> testVersionMinor) Then
            ' version saved on disk Is different from code.  default the saved parameters
            ' configurable parameter variables
            ampTestAccept = DEFAULT_AMP_TEST_ACCEPT_VALUE
            echoTestCnt = DEFAULT_NUM_ECHO_TESTS
            echoTestPassCnt = DEFAULT_NUM_ECHO_TEST_PASS_CNT

            triggerVoltage = DEFAULT_TRIGGER_VOLTAGE
            strVISARsrc = DEFAULT_VISA_ID

            UUTTxGain = DEFAULT_UUT_TX_GAIN
            BRGTXGain = DEFAULT_BRG_TX_GAIN

            logAddr = DEFAULT_LOGICAL_ADDRESS


            RS485_COMPORT = DEFAULT_COM_PORT
            logFile = DEFAULT_LOGFILE

            '// now save this to disk
            saveAppSettings()

        Else

            '// get the version from disk
            ampTestAccept = My.Settings.TMT_AMP_TEST_ACCEPT
            echoTestCnt = My.Settings.TMT_ECHO_TEST_CNT
            echoTestPassCnt = My.Settings.TMT_ECHO_TEST_PASS_CNT


            triggerVoltage = My.Settings.TMT_TRIGGER_VOLTAGE
            strVISARsrc = My.Settings.TMT_VISA_ID

            UUTTxGain = My.Settings.TMT_UUT_TX_GAIN
            BRGTXGain = My.Settings.TMT_BRG_TX_GAIN

            logAddr = My.Settings.TMT_LOGICAL_ADDRESS
            logFile = My.Settings.TMT_LOGFILE


            RS485_COMPORT = My.Settings.TMT_COM_PORT
        End If
    End Sub


    Private Sub SettingsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SettingsToolStripMenuItem.Click
        If (settings.ShowDialog() = DialogResult.OK) Then
            ' now use the values from form2;
            loadAppSettings()
        End If

    End Sub


End Class
