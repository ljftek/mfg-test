﻿Imports System.IO
Imports NationalInstruments.Visa

' to use scope:
' was using extenions references for 
'  nationalinstruments visa
' ividriver

Public Class Form1
    Dim USE_SCOPE As Boolean = True
    'Dim USE_SCOPE As Boolean = False

    'Const USE_RS485 As Boolean = False
    Const USE_RS485 As Boolean = True

    Dim psi As New _
            System.Diagnostics.ProcessStartInfo(“C:\brine\brine.exe”)
    Dim brine As New Process()
    Dim brIPAddr As String = "192.168.70.3"
    Dim PASS_STR As String = "PASS"
    Dim FAIL_STR As String = "FAIL"

    ' scope related
    Dim mbSession As MessageBasedSession
    Dim strVISARsrc As String = "USB0::0x1AB1::0x0588::DS1ET161051698::INSTR" '  //Get VISA resource ID 
    Dim RS485_COMPORT As String  ' com port for talking rs485 to LG3100

    Dim plcAddr As String
    Dim energy As Integer
    Dim meterReading As Double
    Dim lineCount As Integer = 0
    Dim echoRcv As Integer = 0

    Dim UUTTokens() As String = {"Ready", "COMM_NORESP", "PHY", "ECHO SUCCESS", "Response:", "Success", "timeout", "Socket value:"}


    Public Enum uutToks
        READY
        NORESP
        PHY
        ECHO_OK
        RESPONSE
        SUCCESS
        TIMEOUT
        SOCKET_VAL
        UUT_NUM_TOKS
    End Enum

    Const PROCESS_TIMEOUT = 10000
    Const PLC_ADDR_POS = 12  ' index within response str Of PLC address
    Const SOCK_VAL_POS = 15 'index within response str of socket value


    ' defaults for the configurable parameters
    Const DEFAULT_TMT_TEST_VERSION_MAJOR = 0 ' // major version number
    Const DEFAULT_TMT_TEST_VERSION_MINOR = 15 ' // minor version number.  Change this when you add app setting

    Const DEFAULT_NUM_ECHO_TESTS = 5 ' // how many echos For an echo test
    Const DEFAULT_NUM_ECHO_TEST_PASS_CNT = 5
    Const DEFAULT_AMP_TEST_ACCEPT_VALUE = 3.2
    Const DEFAULT_LIGHT_TEST_100_MIN = 8.0
    Const DEFAULT_LIGHT_TEST_100_MAX = 10.0
    Const DEFAULT_LIGHT_TEST_75_MIN = 6.0
    Const DEFAULT_LIGHT_TEST_75_MAX = 8.0
    Const DEFAULT_LIGHT_TEST_50_MIN = 4.0
    Const DEFAULT_LIGHT_TEST_50_MAX = 6.0
    Const DEFAULT_LIGHT_TEST_25_MIN = 2.0
    Const DEFAULT_LIGHT_TEST_25_MAX = 4.0
    Const DEFAULT_LIGHT_TEST_0_MIN = 0.0
    Const DEFAULT_LIGHT_TEST_0_MAX = 2.0

    Const DEFAULT_COLOR_TEST_100_MIN = 10.0
    Const DEFAULT_COLOR_TEST_100_MAX = 10.0
    Const DEFAULT_COLOR_TEST_75_MIN = 10.0
    Const DEFAULT_COLOR_TEST_75_MAX = 10.0
    Const DEFAULT_COLOR_TEST_50_MIN = 10.0
    Const DEFAULT_COLOR_TEST_50_MAX = 10.0
    Const DEFAULT_COLOR_TEST_25_MIN = 10.0
    Const DEFAULT_COLOR_TEST_25_MAX = 10.0
    Const DEFAULT_COLOR_TEST_0_MIN = 10.0
    Const DEFAULT_COLOR_TEST_0_MAX = 10.0

    Const DEFAULT_PWR_PCT = 5

    Const DEFAULT_TRIGGER_VOLTAGE = 2
    Const DEFAULT_VISA_ID = "USB0::0x1AB1::0x0588::DS1ET161051698::INSTR" '  //Get VISA resource ID 

    Const DEFAULT_BRG_TX_GAIN = &HC
    Const DEFAULT_UUT_TX_GAIN = &HC

    Const DEFAULT_LOGICAL_ADDRESS = 1
    ' rs485=12, my usbserial=4
    Const DEFAULT_COM_PORT = "COM12"
    Const DEFAULT_LOGFILE = "c:\mfg-test\TKP_MFG_LOG.txt"

    Dim testVersionMajor As Integer = DEFAULT_TMT_TEST_VERSION_MAJOR
    Dim testVersionMinor As Integer = DEFAULT_TMT_TEST_VERSION_MINOR

    ' configurable parameter variables
    Dim ampTestAccept As Double
    Dim echoTestCnt As Integer
    Dim echoTestPassCnt As Integer

    Dim lightTest100Min As Double
    Dim lightTest100Max As Double
    Dim lightTest75Min As Double
    Dim lightTest75Max As Double
    Dim lightTest50Min As Double
    Dim lightTest50Max As Double
    Dim lightTest25Min As Double
    Dim lightTest25Max As Double
    Dim lightTest0Min As Double
    Dim lightTest0Max As Double

    Dim colorTest100Min As Double
    Dim colorTest100Max As Double
    Dim colorTest75Min As Double
    Dim colorTest75Max As Double
    Dim colorTest50Min As Double
    Dim colorTest50Max As Double
    Dim colorTest25Min As Double
    Dim colorTest25Max As Double
    Dim colorTest0Min As Double
    Dim colorTest0Max As Double

    Dim logAddr As Integer = 1 '  the logical address To use
    Dim logFile As String   ' file to save to

    Dim pwrPct As Integer

    Dim triggerVoltage As Double
    Dim UUTTxGain As Integer
    Dim BRGTXGain As Integer

    Const CMD_GET_ADDR = &H1
    Const CMD_ECHO = &H2
    Const CMD_AMP = &H4
    Const CMD_LIGHT = &H8
    Const CMD_REG = &H10
    Const CMD_SCAN = &H20
    Const CMD_PRINT = &H40
    Const CMD_COLOR = &H80
    Const CMD_SENSOR = &H100
    Const CMD_ENERGY = &H200



    '--------------------------------------------------
    ' application entry point
    '--------------------------------------------------
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        loadAppSettings()
        initScope()
        'getPLCAddr()
        newUUT()
    End Sub
    '--------------------------------------------------
    ' application exit point
    '--------------------------------------------------

    Private Sub main_master_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        saveAppSettings()
        closeScope()
    End Sub


    Function dlgEntry(ByVal caption As String)
        Dim ret As String
        Dim prompt As New Form
        prompt.Width = 200
        prompt.Height = 150
        prompt.Text = ""
        Dim Textbox1 As New TextBox()
        Dim Button1 As New Button()

        Dim lb As New Label
        lb.Text = caption
        lb.Location = New Point(20, 20)


        'TEXTBOX PROPERTIES

        Textbox1.Location = New System.Drawing.Point(20, 50)
        Textbox1.Name = "TextBox1"
        Textbox1.Size = New System.Drawing.Size(140, 20)
        Textbox1.TabIndex = 1
        Textbox1.Show()


        'BUTTON PROPERTIES
        Button1.Location = New System.Drawing.Point(30, 80)
        Button1.Name = "Button1"
        Button1.Size = New System.Drawing.Size(120, 20)
        Button1.TabIndex = 2
        Button1.Text = "OK"
        Button1.DialogResult = DialogResult.OK
        'AddHandler Button1.Click, AddressOf Me.btnClick

        prompt.Controls.Add(lb)
        prompt.Controls.Add(Button1)
        prompt.Controls.Add(Textbox1)

        ' Show testDialog as a modal dialog

        prompt.ShowDialog(Me)

        ret = Textbox1.Text
        prompt.Close()
        prompt.Dispose()

        Return ret.ToUpper()

    End Function

    Private Sub btnClick(sender As Object, e As EventArgs)
        Me.Close()
    End Sub
    Private Sub getPLCAddr()
        runCmd(CMD_GET_ADDR, "")
        If (plcAddr = "") Then
            MessageBox.Show("Unable to retrieve PLC address.")
            plcAddr = " "
        End If

        TextBox_PLC_ADDR.Text = plcAddr
        TextBox_PLC_ADDR.BackColor = Color.White
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button_EXIT.Click

        Application.Exit()
    End Sub


    ' Serial port
    Private Sub writeComPort(ByVal buf() As Byte, cnt As Integer)
        Dim comPort As IO.Ports.SerialPort = Nothing
        Dim buf1(100) As Byte
        Dim ret As Integer
        Dim readBytes, offset, remaining As Integer
        Try
            'comPort = My.Computer.Ports.OpenSerialPort(RS485_COMPORT, 2400)
            comPort = My.Computer.Ports.OpenSerialPort(RS485_COMPORT, 9600)
            comPort.Parity = IO.Ports.Parity.Even
            'comPort.Parity = IO.Ports.Parity.None
            'comPort.Parity = IO.Ports.Parity.Odd

            comPort.StopBits = IO.Ports.StopBits.One
            comPort.DataBits = 8
            comPort.Encoding = System.Text.Encoding.UTF8
            comPort.ReadTimeout = 1000 ' timeout on read
            comPort.Write(buf, 0, cnt)
            ' delay a bit and read
            'Threading.Thread.Sleep(1000)

            readBytes = comPort.Read(buf1, 0, 10)


        Catch ex As TimeoutException
        Finally
            If comPort IsNot Nothing Then comPort.Close()
        End Try
    End Sub

    Function comDataAvail() As Boolean
        ' Receive strings from a serial port.
        ' modbus allows 0 as a broadcast adapter ID.  We use that since we are p2p
        Dim ret As Boolean = False
        ' Dim buf As Byte() = New Byte() {1, 3, &H10, &H20, 0, 1, &H81, &H0} ' baud rate, adapter 1
        Dim buf As Byte() = New Byte() {0, 3, &H20, &H80, 0, 2, &HCF, &HF2} ' power meter, broadcast addr adapter  CF F2

        'Dim buf As Byte() = New Byte() {2, 3, &H20, &H20, 0, 2, &HCE, &H32} '02 03 20 20 00 02 CE 32 ' adapter 2 grid
        'Dim buf As Byte() = New Byte() {1, 3, &H20, &H20, 0, 2, &HCE, 1} '01 03 20 20 00 02 CE 1 ' adapter 1 grid
        'Dim buf As Byte() = New Byte() {1, 3, &H10, &H20, 0, 1, &H81, 0} '01 03 10 20 00 01 81 00 ' adapter 1 baud
        'Dim buf As Byte() = New Byte() {2, 3, &H10, &H20, 0, 1, &H81, &H33} '02 03 10 20 00 01 81 33 ' adapter 2 baud
        'Dim buf As Byte() = New Byte() {2, 3, &H10, &H0, 0, 4, &H40, &HFA} '02 03 10 00 00 04 40 FA ' adapter 2 
        'Dim buf As Byte() = New Byte() {1, 3, 0, 0, 0, 2, &HC4, &HB} ' works on 485-a/d @4800 ,8n1
        'Dim buf As Byte() = New Byte() {1, 3, 0, 0, 0, 2, &HC4, &HC} ' bad chksum on 485-a/d - device wont respond
        writeComPort(buf, 8)
        Dim comPort As IO.Ports.SerialPort = Nothing
        Try
            comPort = My.Computer.Ports.OpenSerialPort(RS485_COMPORT, 9600)
            comPort.Parity = IO.Ports.Parity.Even
            comPort.StopBits = IO.Ports.StopBits.One
            comPort.DataBits = 8
            comPort.Encoding = System.Text.Encoding.UTF8
            comPort.ReadTimeout = 10000 ' timeout on read
            If comPort.BytesToRead > 0 Then
                ret = True
            End If
        Catch ex As TimeoutException
        Finally
            If comPort IsNot Nothing Then comPort.Close()
        End Try
        Return ret
    End Function
    Function readComPort() As String
        ' Receive strings from a serial port.
        Dim returnStr As String = ""

        Dim comPort As IO.Ports.SerialPort = Nothing
        Try
            comPort = My.Computer.Ports.OpenSerialPort(RS485_COMPORT, 9600)
            comPort.Parity = IO.Ports.Parity.Even
            comPort.StopBits = IO.Ports.StopBits.One
            comPort.DataBits = 8
            comPort.Encoding = System.Text.Encoding.UTF8
            comPort.ReadTimeout = 10000 ' timeout on read
            Do
                Dim Incoming As String = comPort.ReadLine()
                If Incoming Is Nothing Then
                    Exit Do
                Else
                    returnStr &= Incoming & vbCrLf
                End If
            Loop
        Catch ex As TimeoutException
            returnStr = "" ' timeout
        Finally
            If comPort IsNot Nothing Then comPort.Close()
        End Try

        Return returnStr
    End Function

    Sub readPwrMeter()
        Dim comPort As IO.Ports.SerialPort = Nothing
        Dim buf As Byte() = New Byte() {0, 3, &H20, &H80, 0, 2, &HCF, &HF2} ' power , broadcast addr adapter  CF F2
        'Dim buf As Byte() = New Byte() {0, 3, &H20, &H0, 0, 2, &HCE, &H1A} ' voltage, broadcast addr adapter  CF F2

        Dim buf1(100) As Byte
        Dim ret As Integer
        Dim readBytes, offset, remaining As Integer
        Try

            comPort = My.Computer.Ports.OpenSerialPort(RS485_COMPORT, 9600)
            comPort.Parity = IO.Ports.Parity.Even
            comPort.StopBits = IO.Ports.StopBits.One
            comPort.DataBits = 8
            comPort.Encoding = System.Text.Encoding.UTF8
            comPort.ReadTimeout = 1000 ' timeout on read
            comPort.Write(buf, 0, 8)
            ' delay a bit and read
            Threading.Thread.Sleep(200)

            readBytes = comPort.Read(buf1, 0, 10)
            If (readBytes = 9) Then
                ' need to reverse the order of the bytes
                buf(3) = buf1(3)
                buf(2) = buf1(4)
                buf(1) = buf1(5)
                buf(0) = buf1(6)
                meterReading = BitConverter.ToSingle(buf, 0)
            End If

        Catch ex As TimeoutException
        Finally
            If comPort IsNot Nothing Then comPort.Close()
        End Try

    End Sub

    Private Sub runCmd(ByVal cmdMask As Integer, param As String)
        ' this routine does all of the actual tests
        Me.Cursor = Cursors.WaitCursor

        brine.StartInfo.FileName = "C:\brine\brine.exe"
        AddHandler brine.OutputDataReceived, AddressOf OutputHandler

        brine.StartInfo.Arguments = " --tcp " & brIPAddr
        brine.StartInfo.UseShellExecute = False
        'brine.StartInfo.UseShellExecute = True
        brine.StartInfo.RedirectStandardOutput = True
        brine.StartInfo.RedirectStandardError = True
        brine.StartInfo.RedirectStandardInput = True
        brine.StartInfo.CreateNoWindow = True
        brine.Start()
        Dim myStreamWriter As StreamWriter = brine.StandardInput
        brine.BeginOutputReadLine()

        ' set log addr 1 and turn off idents
        myStreamWriter.WriteLine("addr log " + logAddr.ToString())
        'myStreamWriter.WriteLine("setclock")

        Threading.Thread.Sleep(300)
        writeScope(":run")
        Threading.Thread.Sleep(300)

        If (cmdMask = CMD_GET_ADDR) Then
            myStreamWriter.WriteLine("cfgread 6 8")
        End If

        If (cmdMask = CMD_ECHO) Then
            Dim gain As String
            gain = UUTTxGain.ToString("X2")
            myStreamWriter.WriteLine("brgwrite 0106 " + gain)
            For i As Integer = 1 To echoTestCnt
                myStreamWriter.WriteLine("echo 0101")
            Next
            myStreamWriter.WriteLine("brgwrite 0106 0E")
        End If

        If (cmdMask = CMD_AMP) Then
            Dim gain As String
            gain = BRGTXGain.ToString("X2")
            myStreamWriter.WriteLine("brwrite 0106 " + gain) ' set bridge txgain to min

            myStreamWriter.WriteLine("echo 0101")
            myStreamWriter.WriteLine("brwrite 0106 0E") ' set bridge txgain to max

        End If

        If (cmdMask = CMD_LIGHT) Then
            myStreamWriter.WriteLine("sockwrite " + param)
        End If
        If (cmdMask = CMD_REG) Then
            myStreamWriter.WriteLine("cfgwrite 0106 EE") ' set tx gain to default
            myStreamWriter.WriteLine("cfgwrite 0107 07") ' set rx gain to default
            myStreamWriter.WriteLine("cfgwrite 100E 01") ' set ident period to default
        End If
        If (cmdMask = CMD_COLOR) Then
            myStreamWriter.WriteLine("sockwrite " + param)
        End If
        If (cmdMask = CMD_SENSOR) Then

        End If

        If (cmdMask = CMD_ENERGY) Then
            myStreamWriter.WriteLine("sockwrite 0 100 0 ") ' turn light on full intensity
            Threading.Thread.Sleep(10000) ' give everything time to settle
            myStreamWriter.WriteLine("sockread 1 ") ' get an energy reading
            readPwrMeter()
            myStreamWriter.WriteLine("sockwrite 0 0 0 ")
        End If

        myStreamWriter.WriteLine("exit")

        brine.WaitForExit(PROCESS_TIMEOUT) ' 10 second timeout
        Console.WriteLine("Done")
        brine.CancelOutputRead()
        RemoveHandler brine.OutputDataReceived, AddressOf OutputHandler
        brine.Close()
        Me.Cursor = Cursors.Default
    End Sub

    Sub setResults(ByVal Label As Label, value As Integer)
        ' set the label to pass/fail based on value
        ' set the back color to green/red
        If (value = 1) Then
            ' passed
            Label.BackColor = Color.Lime
            Label.Text = PASS_STR
        ElseIf (value = 0) Then
            ' Clear it
            Label.Text = ""

        ElseIf (value = -1) Then

            Label.BackColor = Color.Red
            Label.Text = FAIL_STR
        End If

    End Sub
    Private Sub clearAllResults()
        echoRcv = 0

        TextBox_ECHO_CNT.Text = ""
        setResults(ECHO_RESULTS, 0)
        setResults(PLC_RESULTS, 0)
        setResults(INTENSITY_RESULTS, 0)
        setResults(REG_RESULTS, 0)
        setResults(SCAN_RESULTS, 0)
        setResults(COLOR_RESULTS, 0)
        setResults(SENSOR_RESULTS, 0)
        setResults(ENERGY_RESULTS, 0)


        setResults(TEST_RESULT, 0) ' the absolute pass/fail
    End Sub

    Function colorTest(ByVal parm As String, minVal As Double, maxVal As Double)
        Dim logLine As String
        Dim f As Double
        Dim strRead As String
        Dim testResults As Integer = 1 ' pass by default

        writeScope(":TRIGGER:Edge:Sweep Auto")

        runCmd(CMD_LIGHT, parm) ' turn off the light
        Threading.Thread.Sleep(100)
        writeScope(":measure:vaverage? chan2")
        readScope(strRead)
        f = Double.Parse(strRead, System.Globalization.NumberStyles.Float)

        If (f < minVal Or f > maxVal) Then
            testResults = -1
            'setResults(COLOR_RESULTS, -1)
            logLine = "Color Intensity Test " + parm + ": Fail - " + strRead
        Else
            'setResults(COLOR_RESULTS, 1)
            logLine = "Color Intensity Test " + parm + ": Pass - " + strRead
        End If

        logResults(logLine)

        Return testResults
    End Function

    Function lightTest(ByVal parm As String, minVal As Double, maxVal As Double)
        Dim logLine As String
        Dim f As Double
        Dim strRead As String
        Dim testResults As Integer = 1 ' pass by default

        writeScope(":TRIGGER:Edge:Sweep Auto")

        runCmd(CMD_LIGHT, parm) ' turn off the light
        Threading.Thread.Sleep(100)
        writeScope(":measure:vaverage? chan2")
        readScope(strRead)
        f = Double.Parse(strRead, System.Globalization.NumberStyles.Float)

        If (f < minVal Or f > maxVal) Then
            testResults = -1
            'setResults(INTENSITY_RESULTS, -1)
            logLine = "Light Intensity Test " + parm + ": Fail - " + strRead
        Else
            'setResults(INTENSITY_RESULTS, 1)
            logLine = "Light Intensity Test " + parm + ": Pass - " + strRead
        End If

        logResults(logLine)

        Return testResults
    End Function

    Private Sub Button_RUN_Click(sender As Object, e As EventArgs) Handles Button_RUN.Click
        Dim cmd As Integer = 0
        Dim relay As Integer
        Dim testResults As Integer = 1 ' pass by default
        Dim f As Double
        Dim strRead As String

        echoRcv = 0

        TextBox_ECHO_CNT.Text = ""
        clearAllResults()

        Dim runTime As Date = Date.Now
        Dim logLine As String

        logLine = "Test Ver:" + testVersionMajor.ToString() + "." + testVersionMinor.ToString() + " Execution Date:" + runTime.ToShortDateString() + " Time: " + runTime.ToShortTimeString() + " PLC Addr: " + plcAddr.ToString()
        logResults(logLine)
        If (USE_SCOPE = False) Then
            logLine = "No Scope"
            logResults(logLine)
        End If

        If (CheckBox_ECHO.Checked = True) Then
            cmd = CMD_ECHO
            writeScope(":CHANnel1:Display On")
            Threading.Thread.Sleep(300)
            writeScope(":CHANnel2:Display Off")
            Threading.Thread.Sleep(300)
            runCmd(cmd, "")


            TextBox_ECHO_CNT.Text = " " + echoRcv.ToString() + " out of " + echoTestCnt.ToString()
            If (echoTestCnt = echoRcv) Then
                logLine = "Echo Test: Pass - " + echoRcv.ToString() + " out of " + echoTestCnt.ToString()
                setResults(ECHO_RESULTS, 1)
            Else
                logLine = "Echo Test: Fail - " + echoRcv.ToString() + " out of " + echoTestCnt.ToString()
                setResults(ECHO_RESULTS, -1)
                testResults = -1
            End If
            logResults(logLine)
        End If

        If (CheckBox_AMP.Checked = True) Then
            cmd = CMD_AMP
            writeScope(":CHANnel1:Display On")
            Threading.Thread.Sleep(300)
            writeScope(":CHANnel2:Display Off")
            Threading.Thread.Sleep(300)
            writeScope(":TRIGGER:Edge:Sweep Single")
            Threading.Thread.Sleep(300)
            writeScope(":TRIGGER:EDGE:Coupling AC")
            runCmd(cmd, "")

            writeScope(":measure:vpp? chan1")

            readScope(strRead)

            ' when we read the value from the scope, we get a string in scientific notation
            ' ie: 99e36
            f = Double.Parse(strRead, System.Globalization.NumberStyles.Float)
            If (f < ampTestAccept) Then
                testResults = -1
                setResults(PLC_RESULTS, -1)
                logLine = "Amp Test: Fail - " + strRead
            Else
                setResults(PLC_RESULTS, 1)
                logLine = "Amp Test: Pass - " + strRead
            End If

            logResults(logLine)
            writeScope(":TRIGGER:Edge:Sweep Single")

        End If



        If (CheckBox_LIGHT.Checked = True) Then
            cmd = CMD_LIGHT
            Dim liRes As Integer = 1

            writeScope(":CHANnel1:Display Off")
            Threading.Thread.Sleep(300)
            writeScope(":CHANnel2:Display On")
            Threading.Thread.Sleep(300)
            writeScope(":TRIGGER:EDGE:Coupling DC")
            ' command |= CMD_LIGHT;
            If (lightTest("0 0 0", lightTest0Min, lightTest0Max) = -1) Then
                liRes = -1
            End If
            If (lightTest("0 25 0", lightTest25Min, lightTest25Max) = -1) Then
                liRes = -1
            End If
            If (lightTest("0 50 0", lightTest50Min, lightTest50Max) = -1) Then
                liRes = -1
            End If
            If (lightTest("0 75 0", lightTest75Min, lightTest75Max) = -1) Then
                liRes = -1
            End If
            If (lightTest("0 100 0", lightTest100Min, lightTest100Max) = -1) Then
                liRes = -1
            End If

            setResults(INTENSITY_RESULTS, liRes)
            If (liRes = -1) Then
                testResults = liRes
            End If
            runCmd(cmd, "0 0 0") ' turn off the light 
        End If

        If (CheckBox_COLOR.Checked = True) Then
            cmd = CMD_COLOR

            Dim liRes As Integer = 1

            writeScope(":CHANnel1:Display Off")
            Threading.Thread.Sleep(300)
            writeScope(":CHANnel2:Display On")
            Threading.Thread.Sleep(300)
            writeScope(":TRIGGER:EDGE:Coupling DC")

            If (colorTest("3 0 0", colorTest0Min, colorTest0Max) = -1) Then
                liRes = -1
            End If
            If (colorTest("3 25 0", colorTest25Min, colorTest25Max) = -1) Then
                liRes = -1
            End If
            If (colorTest("3 50 0", colorTest50Min, colorTest50Max) = -1) Then
                liRes = -1
            End If
            If (colorTest("3 75 0", colorTest75Min, colorTest75Max) = -1) Then
                liRes = -1
            End If
            If (colorTest("3 100 0", colorTest100Min, colorTest100Max) = -1) Then
                liRes = -1
            End If

            setResults(COLOR_RESULTS, liRes)
            If (liRes = -1) Then
                testResults = liRes
            End If
            runCmd(cmd, "0 0 0") ' turn off the light 


        End If


        If (CheckBox_REG.Checked = True) Then
            cmd = CMD_REG
            runCmd(cmd, "")
        End If

        If (CheckBox_SENSOR.Checked = True) Then
            cmd = CMD_SENSOR

            'runCmd(cmd, "")
            relay = MsgBox("Cover Light Sensor.   Did relay close?", MsgBoxStyle.YesNo)
            If (relay = vbYes) Then
                'runCmd(cmd, "")
                relay = MsgBox("Uncover Light Sensor.  Did relay open?", MsgBoxStyle.YesNo)
                If (relay = vbNo) Then
                    testResults = -1
                    logLine = "Sensor Test: Fail - Relay did not open."
                    setResults(SENSOR_RESULTS, -1)
                Else
                    logLine = "Sensor Test: Pass"
                    setResults(SENSOR_RESULTS, 1)

                End If
            Else
                testResults = -1
                setResults(SENSOR_RESULTS, -1)

                logLine = "Sensor Test: Fail - Relay did not close."
            End If

            logResults(logLine)
        End If
        If (CheckBox_ENERGY.Checked = True) Then
            cmd = CMD_ENERGY
            energy = 0
            meterReading = 0
            runCmd(cmd, "")
            Threading.Thread.Sleep(2000)
            If (energy <> 0 And meterReading <> 0) Then
                meterReading = meterReading * 10000
                Dim onePerMeter As Double
                onePerMeter = pwrPct * (meterReading / 100)

                If (((meterReading - onePerMeter) <= energy) And (energy <= (meterReading + onePerMeter))) Then
                    'pass
                    setResults(ENERGY_RESULTS, 1)
                    logLine = "Energy Test: Pass" + " Meter: " + meterReading.ToString() + " UUT Energy: " + energy.ToString()
                Else
                    'fail
                    setResults(ENERGY_RESULTS, -1)
                    testResults = -1
                    logLine = "Energy Test: fail" + " Meter: " + meterReading.ToString() + " UUT Energy: " + energy.ToString()
                End If
            Else
                ' no reading 
                logLine = "meter: " + meterReading.ToString() + " UUT energy: " + energy.ToString()
                MsgBox(logLine, vbOKOnly)
                setResults(ENERGY_RESULTS, -1)
                testResults = -1
                logLine = "Energy Test: fail.  Unable to read energy"

            End If
            logResults(logLine)
        End If

        If (CheckBox_SCAN.Checked = True) Then

            cmd = CMD_SCAN
            Dim scanPLC As String = dlgEntry("PLC Address")

            If (scanPLC.Equals(plcAddr)) Then
                setResults(SCAN_RESULTS, 1)
                logLine = "Scan Test: pass PLC addr: " + plcAddr.ToString() + " Scanned Addr: " + scanPLC.ToString()

            Else
                logLine = "Scan Test: fail PLC addr: " + plcAddr.ToString() + " Scanned Addr: " + scanPLC.ToString()

                setResults(SCAN_RESULTS, -1)
                testResults = -1
            End If
            logResults(logLine)
        End If



        If (CheckBox_PRINT.Checked = True) Then
            cmd = CMD_PRINT
        End If

        setResults(TEST_RESULT, testResults)

    End Sub

    Sub OutputHandler(sender As Object, e As DataReceivedEventArgs)
        Dim linTok As Int32
        If Not String.IsNullOrEmpty(e.Data) Then
            lineCount += 1

            ' Add the text to the collected output.
            Console.WriteLine(e.Data)
            linTok = parseUUT(e.Data)
            Console.WriteLine(linTok)
            Select Case linTok
                Case uutToks.READY
                Case uutToks.NORESP
                Case uutToks.PHY
                Case uutToks.ECHO_OK
                    echoRcv = echoRcv + 1
                Case uutToks.RESPONSE
                    ' PLC addr - format Is Response 16hexchars
                    Dim addr As String = e.Data.Substring(PLC_ADDR_POS)
                    plcAddr = addr.ToUpper()
                Case uutToks.SOCKET_VAL
                    Dim energyStr As String = e.Data.Substring(SOCK_VAL_POS)
                    energy = Convert.ToInt32(energyStr)
                Case uutToks.SUCCESS
                Case uutToks.TIMEOUT
                    Console.WriteLine(e.Data)
            End Select
        End If
    End Sub



    Private Function parseUUT(ByVal bridgeResp As String)

        For i As Integer = 0 To uutToks.UUT_NUM_TOKS - 1
            Dim b As Boolean = bridgeResp.Contains(UUTTokens(i))
            If (b) Then
                Return i
            End If
        Next

        Return -1
    End Function

    Public Sub saveAppSettings()

        ' load the configurable parameters

        My.Settings.TMT_TEST_VERSION_MAJOR = testVersionMajor
        My.Settings.TMT_TEST_VERSION_MINOR = testVersionMinor
        My.Settings.TMT_AMP_TEST_ACCEPT = ampTestAccept
        My.Settings.TMT_ECHO_TEST_CNT = echoTestCnt
        My.Settings.TMT_ECHO_TEST_PASS_CNT = echoTestPassCnt
        My.Settings.TMT_LIGHT_TEST_100_MIN = lightTest100Min
        My.Settings.TMT_LIGHT_TEST_100_MAX = lightTest100Max
        My.Settings.TMT_LIGHT_TEST_75_MIN = lightTest75Min
        My.Settings.TMT_LIGHT_TEST_75_MAX = lightTest75Max
        My.Settings.TMT_LIGHT_TEST_50_MIN = lightTest50Min
        My.Settings.TMT_LIGHT_TEST_50_MAX = lightTest50Max
        My.Settings.TMT_LIGHT_TEST_25_MIN = lightTest25Min
        My.Settings.TMT_LIGHT_TEST_25_MAX = lightTest25Max
        My.Settings.TMT_LIGHT_TEST_0_MIN = lightTest0Min
        My.Settings.TMT_LIGHT_TEST_0_MAX = lightTest0Max

        My.Settings.TMT_COLOR_TEST_100_MIN = colorTest100Min
        My.Settings.TMT_COLOR_TEST_100_MAX = colorTest100Max
        My.Settings.TMT_COLOR_TEST_75_MIN = colorTest75Min
        My.Settings.TMT_COLOR_TEST_75_MAX = colorTest75Max
        My.Settings.TMT_COLOR_TEST_50_MIN = colorTest50Min
        My.Settings.TMT_COLOR_TEST_50_MAX = colorTest50Max
        My.Settings.TMT_COLOR_TEST_25_MIN = colorTest25Min
        My.Settings.TMT_COLOR_TEST_25_MAX = colorTest25Max
        My.Settings.TMT_COLOR_TEST_0_MIN = colorTest0Min
        My.Settings.TMT_COLOR_TEST_0_MAX = colorTest0Max

        My.Settings.TMT_LOGICAL_ADDRESS = logAddr
        My.Settings.TMT_PWR_PCT = pwrPct

        My.Settings.TMT_COM_PORT = RS485_COMPORT
        My.Settings.TMT_LOGFILE = logFile

        My.Settings.TMT_TRIGGER_VOLTAGE = triggerVoltage
        My.Settings.TMT_VISA_ID = strVISARsrc

        My.Settings.TMT_UUT_TX_GAIN = UUTTxGain
        My.Settings.TMT_BRG_TX_GAIN = BRGTXGain

        My.Settings.Save() ' // save the configurable parameters
        My.Settings.Upgrade()
    End Sub

    Private Sub loadAppSettings()

        ' load the configurable parameters

        Dim diskVersionMajor As Integer = My.Settings.TMT_TEST_VERSION_MAJOR
        Dim diskVersionMinor As Integer = My.Settings.TMT_TEST_VERSION_MINOR
        '// what are the settings from disk?  If this Is a different version, load defaults
        If (diskVersionMajor <> testVersionMajor Or diskVersionMinor <> testVersionMinor) Then
            ' version saved on disk Is different from code.  default the saved parameters
            ' configurable parameter variables
            ampTestAccept = DEFAULT_AMP_TEST_ACCEPT_VALUE
            echoTestCnt = DEFAULT_NUM_ECHO_TESTS
            echoTestPassCnt = DEFAULT_NUM_ECHO_TEST_PASS_CNT
            lightTest100Min = DEFAULT_LIGHT_TEST_100_MIN
            lightTest100Max = DEFAULT_LIGHT_TEST_100_MAX
            lightTest75Min = DEFAULT_LIGHT_TEST_75_MIN
            lightTest75Max = DEFAULT_LIGHT_TEST_75_MAX
            lightTest50Min = DEFAULT_LIGHT_TEST_50_MIN
            lightTest50Max = DEFAULT_LIGHT_TEST_50_MAX
            lightTest25Min = DEFAULT_LIGHT_TEST_25_MIN
            lightTest25Max = DEFAULT_LIGHT_TEST_25_MAX
            lightTest0Min = DEFAULT_LIGHT_TEST_0_MIN
            lightTest0Max = DEFAULT_LIGHT_TEST_0_MAX

            colorTest100Min = DEFAULT_COLOR_TEST_100_MIN
            colorTest100Max = DEFAULT_COLOR_TEST_100_MAX
            colorTest75Min = DEFAULT_COLOR_TEST_75_MIN
            colorTest75Max = DEFAULT_COLOR_TEST_75_MAX
            colorTest50Min = DEFAULT_COLOR_TEST_50_MIN
            colorTest50Max = DEFAULT_COLOR_TEST_50_MAX
            colorTest25Min = DEFAULT_COLOR_TEST_25_MIN
            colorTest25Max = DEFAULT_COLOR_TEST_25_MAX
            colorTest0Min = DEFAULT_COLOR_TEST_0_MIN
            colorTest0Max = DEFAULT_COLOR_TEST_0_MAX

            triggerVoltage = DEFAULT_TRIGGER_VOLTAGE
            strVISARsrc = DEFAULT_VISA_ID

            UUTTxGain = DEFAULT_UUT_TX_GAIN
            BRGTXGain = DEFAULT_BRG_TX_GAIN

            logAddr = DEFAULT_LOGICAL_ADDRESS
            pwrPct = DEFAULT_PWR_PCT

            RS485_COMPORT = DEFAULT_COM_PORT
            logFile = DEFAULT_LOGFILE

            '// now save this to disk
            saveAppSettings()

        Else

            '// get the version from disk
            ampTestAccept = My.Settings.TMT_AMP_TEST_ACCEPT
            echoTestCnt = My.Settings.TMT_ECHO_TEST_CNT
            echoTestPassCnt = My.Settings.TMT_ECHO_TEST_PASS_CNT


            lightTest100Min = My.Settings.TMT_LIGHT_TEST_100_MIN
            lightTest100Max = My.Settings.TMT_LIGHT_TEST_100_MAX
            lightTest75Min = My.Settings.TMT_LIGHT_TEST_75_MIN
            lightTest75Max = My.Settings.TMT_LIGHT_TEST_75_MAX
            lightTest50Min = My.Settings.TMT_LIGHT_TEST_50_MIN
            lightTest50Max = My.Settings.TMT_LIGHT_TEST_50_MAX
            lightTest25Min = My.Settings.TMT_LIGHT_TEST_25_MIN
            lightTest25Max = My.Settings.TMT_LIGHT_TEST_25_MAX
            lightTest0Min = My.Settings.TMT_LIGHT_TEST_0_MIN
            lightTest0Max = My.Settings.TMT_LIGHT_TEST_0_MAX


            colorTest100Min = My.Settings.TMT_COLOR_TEST_100_MIN
            colorTest100Max = My.Settings.TMT_COLOR_TEST_100_MAX
            colorTest75Min = My.Settings.TMT_COLOR_TEST_75_MIN
            colorTest75Max = My.Settings.TMT_COLOR_TEST_75_MAX
            colorTest50Min = My.Settings.TMT_COLOR_TEST_50_MIN
            colorTest50Max = My.Settings.TMT_COLOR_TEST_50_MAX
            colorTest25Min = My.Settings.TMT_COLOR_TEST_25_MIN
            colorTest25Max = My.Settings.TMT_COLOR_TEST_25_MAX
            colorTest0Min = My.Settings.TMT_COLOR_TEST_0_MIN
            colorTest0Max = My.Settings.TMT_COLOR_TEST_0_MAX

            triggerVoltage = My.Settings.TMT_TRIGGER_VOLTAGE
            strVISARsrc = My.Settings.TMT_VISA_ID

            UUTTxGain = My.Settings.TMT_UUT_TX_GAIN
            BRGTXGain = My.Settings.TMT_BRG_TX_GAIN

            logAddr = My.Settings.TMT_LOGICAL_ADDRESS
            logFile = My.Settings.TMT_LOGFILE
            pwrPct = My.Settings.TMT_PWR_PCT

            RS485_COMPORT = My.Settings.TMT_COM_PORT
        End If
    End Sub



    Private Sub SettingsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SettingsToolStripMenuItem.Click
        'settings.ShowDialog(this);
        If (settings.ShowDialog() = DialogResult.OK) Then
            ' now use the values from form2;
            loadAppSettings()
        End If

    End Sub
    Private Sub newUUT()
        ' new uut
        getPLCAddr()
        'runCmd(CMD_LIGHT, "0 0 0") ' turn off the light

        clearAllResults()
    End Sub
    Private Sub Button2_Click_1(sender As Object, e As EventArgs) Handles Button2.Click
        newUUT()

    End Sub

    Private Sub logResults(ByVal logLine As String)
        Dim path As String = "c:\mfg-test"

        Try
            ' Determine whether the directory exists.
            If (Directory.Exists(path) = False) Then
                ' Try to create the directory.
                Dim di As DirectoryInfo = Directory.CreateDirectory(path)
                Console.WriteLine("The directory was created successfully at {0}.", Directory.GetCreationTime(path))
            End If
        Catch e As Exception
            Console.WriteLine("The process failed: {0}.", e.ToString())
        End Try


        ' append a line to the logfile
        Dim objWriter As New System.IO.StreamWriter(logFile, True)

        objWriter.WriteLine(logLine)
        'My.Computer.FileSystem.WriteAllText(logFile, logLine, True)

        objWriter.Close()

    End Sub

    '-----------------------------------------------------------------------------------
    Private Sub readScope(ByRef parm As String)
        If (USE_SCOPE) Then
            parm = mbSession.RawIO.ReadString()
        Else
            parm = "-99"
        End If
    End Sub

    Private Sub writeScope(ByVal parm As String)
        If (USE_SCOPE) Then
            mbSession.RawIO.Write(parm)
        End If

    End Sub

    Private Sub initScope()
        If USE_SCOPE Then

            Try

                mbSession = New UsbSession(strVISARsrc, Ivi.Visa.AccessModes.LoadConfig, 10000, True) 'Instantiate And open a Message Based VISA Session
                mbSession.TerminationCharacterEnabled = True ' Important
                Dim textToWrite As String = ""
                Dim strRead As String

                writeScope("*idn?")
                readScope(strRead)


                writeScope(":CHANnel1:Display On")
                Threading.Thread.Sleep(300)
                writeScope(":CHANnel2:Display On")
                Threading.Thread.Sleep(300)
                ' set up channel 1

                Threading.Thread.Sleep(300)
                writeScope(":Channel1:BWlimit off")
                Threading.Thread.Sleep(300)
                writeScope(":channel1:probe 1") ' 1x
                Threading.Thread.Sleep(300)
                writeScope(":TRIGGER:EDGE:SOURCE CHANnel1")
                Threading.Thread.Sleep(300)
                writeScope(":TRIGGER:EDGE:Sweep Normal")
                Threading.Thread.Sleep(300)
                'writeScope(":TRIGGER:EDGE:level 2")
                writeScope(":TRIGGER:EDGE:level " + triggerVoltage.ToString())

                Threading.Thread.Sleep(300)
                writeScope(":channel1:offset 0")
                Threading.Thread.Sleep(300)
                'writeScope(":channel1:scale 5")
                Threading.Thread.Sleep(300)

                Threading.Thread.Sleep(300)
                writeScope(":timebase:offset 0")
                Threading.Thread.Sleep(300)
                writeScope(":TIMebase:SCALe 0.05")
                Threading.Thread.Sleep(300)

                ' set up channel 2

                writeScope(":TRIGGER:EDGE:Coupling DC")
                Threading.Thread.Sleep(300)
                writeScope(":TRIGger:EDGE:SWEep AUTO CHANnel2")
                Threading.Thread.Sleep(300)
                writeScope(":measure:vaverage? chan2")
                Threading.Thread.Sleep(300)
                writeScope(":channel2:offset -4")
                Threading.Thread.Sleep(300)
                writeScope(":channel2:scale 2")
                Threading.Thread.Sleep(300)
                writeScope(":channel2:probe 10")

                Threading.Thread.Sleep(300)
                writeScope(":run")

            Catch ex As Ivi.Visa.NativeVisaException
                MessageBox.Show("Unable to connect to oscilloscope.  Is it plugged into the USB port?")
                USE_SCOPE = False

            End Try
        End If

    End Sub
    Private Sub closeScope()
        Try

            ' close the scope session

            writeScope(":key:forc")
            mbSession.Clear()
            mbSession.Dispose() 'shut it down
        Catch
        End Try

    End Sub

    Private Sub ToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem1.Click
        newUUT()

    End Sub
End Class